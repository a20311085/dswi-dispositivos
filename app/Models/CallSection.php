<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallSection extends Model
{
    use HasFactory;
    protected $table = 'callsections';

    protected $fillable = [
        'title',
        'paragraph',
        'image'
    ];

}
