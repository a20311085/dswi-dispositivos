<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dispositivo;

class Especificacion extends Model
{
    use HasFactory;
        protected $table = 'especificaciones';

    
    protected $fillable = [
        'almacenamiento', 
        'ram', 
        'pantalla', 
        'sistema_operativo', 
        'bateria', 
        'procesador', 
        'color', 
        'dimensiones', 
        'camaras',
        'status',
        'dispositivos_id',
    ];


    public function dispositivo() {
        return $this->belongsTo(Dispositivo::class, 'dispositivos_id', 'id');
    }
}
