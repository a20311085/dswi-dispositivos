<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Fabricante;
use App\Models\Especificacion;
use App\Models\Categoria;
use App\Models\Existencia;



class Dispositivo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_dispositivo', 
        'precio', 
        'image',
        'fabricante_id', 
        'categoria_id', 
        'status',
        'especificacion_id',
        'existencia_id',

    ];  

    public function fabricante() {
        return $this->belongsTo(Fabricante::class, 'fabricante_id', 'id');
    }
    public function categoria() {
        return $this->belongsTo(Categoria::class, 'categoria_id', 'id');
    }
    public function especificacion() {
        return $this->belongsTo(Especificacion::class, 'especificacion_id', 'id');
    }
    public function existencia() {
        return $this->belongsTo(Existencia::class, 'existencia_id', 'id');
    }
   
   


}
