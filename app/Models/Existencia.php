<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dispositivo;


class Existencia extends Model
{
    use HasFactory;

    protected $fillable = [
        'cantidad_disponible',
        'status',
        'dispositivos_id',
    ];

    
    public function dispositivo() {
        return $this->belongsTo(Dispositivo::class, 'dispositivos_id', 'id');
    }
}
