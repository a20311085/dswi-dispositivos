<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Dispositivo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DispositivoController extends Controller
{
    public function list() {

        $dispositivos = Dispositivo::where('status', '=', 1)->get();


		$list = [];

        foreach ($dispositivos as $dispositivo) {

            $object = [
                
                'id' => $dispositivo ->id,
                'nombre_dispositivo' => $dispositivo->nombre_dispositivo,
                'precio'=> $dispositivo ->precio,
                'image'=> $dispositivo ->image,
                'fabricante_id'=> $dispositivo ->fabricante->nombre_fabricante,
                'categoria_id'=> $dispositivo->categoria->nombre_categoria,
                'especificacion_id'=> $dispositivo->especificacion_id,
                'existencia_id'=> $dispositivo->existencia_id,

                'created_at' => $dispositivo->created_at ? $dispositivo->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $dispositivo->updated_at ? $dispositivo->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $dispositivo = Dispositivo::where('id', '=', $id)->first();
        
        $object = [

            'id' => $dispositivo ->id,
            'nombre_dispositivo' => $dispositivo->nombre_dispositivo,
            'precio'=> $dispositivo ->precio,
            'image'=> $dispositivo ->image,
            'fabricante_id'=> $dispositivo ->fabricante->nombre_fabricante,
            'categoria_id'=> $dispositivo->categoria->nombre_categoria,
            'especificacion_id'=> $dispositivo->especificacion_id,
            'existencia_id'=> $dispositivo->existencia_id,
            'created_at' => $dispositivo->created_at ? $dispositivo->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $dispositivo->updated_at ? $dispositivo->updated_at->diffForHumans() : 'Fecha no disponible',

        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        

        $data = $request->validate([

            'nombre_dispositivo' => 'required',
            'precio'=>  'required',
            'image' => 'required',
            'fabricante_id' => 'required|numeric',
            'categoria_id' => 'required|numeric',
            'especificacion_id' => 'required|numeric',
            'existencia_id' => 'required|numeric',
          

        ]);


        $dispositivo = Dispositivo::create([

            'nombre_dispositivo' => $data['nombre_dispositivo'],
            'precio' => $data['precio'],
            'image' => $data['image'],
            'fabricante_id' => $data['fabricante_id'],
            'categoria_id' => $data['categoria_id'],
            'especificacion_id' => $data['especificacion_id'],
            'existencia_id' => $data['existencia_id'],
       
        ]);


        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Dispositivo correctamente',
            'data' => $dispositivo
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $dispositivo = Dispositivo::where('id', '=', $request->id)->first();

        $data = $request->validate([
            'status' => 'integer'
        ]);

        $dispositivo->status = $data['status'];
        $dispositivo->save();


        $object = [
            'response' => 'ok',
            'message' => 'El Dispositivo se ha sido inhabilitado correctamente.',
            'data' => $dispositivo
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $dispositivo = Dispositivo::where('id', '=', $request->id)->first();

        //validar la información
        $data = $request->validate([
            'nombre_dispositivo' => 'required',
            'precio'=>  'required',
            'image' => 'required',
            'fabricante_id' => 'required|numeric',
            'categoria_id' => 'required|numeric',
            'especificacion_id' => 'required|numeric',
            'existencia_id' => 'required|numeric',
        ]);

        $dispositivo->nombre_dispositivo = $data['nombre_dispositivo'];
        $dispositivo->precio = $data['precio'];
        $dispositivo->image = $data['image'];
        $dispositivo->fabricante_id = $data['fabricante_id'];
        $dispositivo->categoria_id = $data['categoria_id'];
        $dispositivo->especificacion_id = $data['especificacion_id'];
        $dispositivo->existencia_id = $data['existencia_id'];
       

        $dispositivo->save();

        // try catch y el arreglo object se cambia con los datos del error

        $object = [
            'response' => 'ok',
            'message' => 'El Dispostivo ha sido modificado correctamente.',
            'data' => $dispositivo
        ];

        return response()->json($object);
    }



}
