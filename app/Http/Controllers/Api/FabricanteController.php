<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Fabricante;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FabricanteController extends Controller
{
    public function list() {

        $fabricantes = Fabricante::where('status', '=', 1)->get();


		$list = [];

        foreach ($fabricantes as $fabricante) {

            $object = [
                
                'id' => $fabricante ->id,
                'nombre_fabricante' => $fabricante->nombre_fabricante,
                'pais_origen'=> $fabricante ->pais_origen,
                'created_at' => $fabricante->created_at ? $fabricante->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $fabricante->updated_at ? $fabricante->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $fabricantes = Fabricante::where('id', '=', $id)->first();
        
        $object = [
            'id' => $fabricantes ->id,
            'nombre_fabricante' => $fabricantes->nombre_fabricante,
            'pais_origen'=> $fabricantes ->pais_origen,
            'created_at' => $fabricantes->created_at ? $fabricantes->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $fabricantes->updated_at ? $fabricantes->updated_at->diffForHumans() : 'Fecha no disponible',
    
        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        
        $data = $request->validate([

            'nombre_fabricante' => 'required',
            'pais_origen' => 'required',
        ]);

        $fabricantes = Fabricante::create([

            'nombre_fabricante' => $data['nombre_fabricante'],
            'pais_origen' => $data['pais_origen'],

        ]);


        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Fabricante correctamente',
            'data' => $fabricantes
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $fabricantes = Fabricante::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'status' => 'integer'
        ]);



        $fabricantes->status = $data['status'];
        $fabricantes->save();

 

        $object = [
            'response' => 'ok',
            'message' => 'El Fabricante ha sido inhabilitado correctamente.',
            'data' => $fabricantes
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $fabricantes = Fabricante::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'nombre_fabricante' => 'required',
            'pais_origen' => 'required',
        ]);

        
        $fabricantes->nombre_fabricante = $data['nombre_fabricante'];
        $fabricantes->pais_origen = $data['pais_origen'];


        $fabricantes->save();

        $object = [
            'response' => 'ok',
            'message' => 'El Fabricante ha sido modificado correctamente.',
            'data' => $fabricantes
        ];

        return response()->json($object);
    }
}
