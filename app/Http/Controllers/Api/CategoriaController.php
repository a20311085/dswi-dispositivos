<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
   
    public function list() {

        $categorias = Categoria::where('status', '=', 1)->get();


		$list = [];

        foreach ($categorias as $categoria) {

            $object = [
                
                'id' => $categoria ->id,
                'nombre_categoria' => $categoria->nombre_categoria,
                'descripcion_categoria'=> $categoria ->descripcion_categoria,
                'created_at' => $categoria->created_at ? $categoria->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $categoria->updated_at ? $categoria->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $categoria = Categoria::where('id', '=', $id)->first();
        
        $object = [
            'id' => $categoria ->id,
            'nombre_categoria' => $categoria->nombre_categoria,
            'descripcion_categoria'=> $categoria ->descripcion_categoria,
            'created_at' => $categoria->created_at ? $categoria->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $categoria->updated_at ? $categoria->updated_at->diffForHumans() : 'Fecha no disponible',
    
        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        

        $data = $request->validate([

            'nombre_categoria' => 'required',
            'descripcion_categoria' => 'required',
        ]);


        $categoria = Categoria::create([

            'nombre_categoria' => $data['nombre_categoria'],
            'descripcion_categoria' => $data['descripcion_categoria'],

        ]);

        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Categoria correctamente',
            'data' => $categoria
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $categorias = Categoria::where('id', '=', $request->id)->first();

        //validar la información
        $data = $request->validate([
            'status' => 'integer'
        ]);

        //verificar si el campo nullable viene o no viene

        // en un if guardar según el caso

        $categorias->status = $data['status'];
        $categorias->save();

        // try catch y el arreglo object se cambia con los datos del error

        $object = [
            'response' => 'ok',
            'message' => 'El Usuario ha sido inhabilitado correctamente.',
            'data' => $categorias
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $categorias = Categoria::where('id', '=', $request->id)->first();

        //validar la información
        $data = $request->validate([
            'nombre_categoria' => 'required',
            'descripcion_categoria' => 'required',
        ]);

        //verificar si el campo nullable viene o no viene

        // en un if guardar según el caso
        
        $categorias->nombre_categoria = $data['nombre_categoria'];
        $categorias->descripcion_categoria = $data['descripcion_categoria'];

        $categorias->save();

        // try catch y el arreglo object se cambia con los datos del error

        $object = [
            'response' => 'ok',
            'message' => 'La Categoria ha sido modificado correctamente.',
            'data' => $categorias
        ];

        return response()->json($object);
    }






}
