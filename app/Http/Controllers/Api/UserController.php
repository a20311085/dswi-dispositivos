<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function list()
    {

        $usuarios = User::where('status', '=', 1)->get();


        $list = [];

        foreach ($usuarios as $usuario) {

            $object = [

                'id' => $usuario->id,
                'email' => $usuario->email,
                'phone' => $usuario->phone,
                'email' => $usuario->email,
                'avatar' => asset($usuario->avatar),
                'level_id' => $usuario->level->code,
                //'password'=> $usuario ->password,
                'created_at' => $usuario->created_at ? $usuario->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $usuario->updated_at ? $usuario->updated_at->diffForHumans() : 'Fecha no disponible',

            ];

            array_push($list, $object);
        }

        return response()->json($list);
    }

    public function item($id)
    {

        $usuario = User::where('id', '=', $id)->first();

        $object = [

            'id' => $usuario->id,
            'email' => $usuario->email,
            'phone' => $usuario->phone,
            'email' => $usuario->email,
            'avatar' => asset($usuario->avatar),
            'level_id' => $usuario->level->code,
            //'password'=> $usuario ->password,
            'created_at' => $usuario->created_at ? $usuario->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $usuario->updated_at ? $usuario->updated_at->diffForHumans() : 'Fecha no disponible',

        ];

        return response()->json($object);
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'avatar' => 'nullable|image|max:3000',
            'email' => 'required|unique:users',
            'phone' => 'required',
            'level_id' => 'required|numeric',
        ]);

        $user = new User();

        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];
        $user->phone = $validatedData['phone'];
        $user->level_id = $validatedData['level_id'];

        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar')->store('public/image');
            $avatarPath = Storage::url($path);
            $user->avatar = $avatarPath;
        }

        if ($user->save()) {
            return response()->json(['message' => 'Usuario agregado con éxito', 'data' => $user], 201);
        } else {
            return response()->json(['message' => 'Error al agregar usuario'], 500);
        }
    }

    public function deactivate(Request $request) {    

        $user = User::where('id', '=', $request->id)->first();

        $data = $request->validate([
            'status' => 'integer'
        ]);


        $user->status = $data['status'];
        $user->save();


        $object = [
            'response' => 'ok',
            'message' => 'La Categoria ha sido inhabilitado correctamente.',
            'data' => $user
        ];

        return response()->json($object);
    }



    public function update(Request $request)
{
    $id = $request->id; // Obtener el ID del cuerpo de la solicitud

    $user = User::where('id', '=', $id)->first();

    if (!$user) {
        return response()->json(['message' => 'Usuario no encontrado'], 404);
    }

    $validatedData = $request->validate([
        'name' => 'required',
        'avatar' => 'nullable|image|max:3000',
        'email' => 'required|unique:users,email,' . $id,
        'phone' => 'required',
        'level_id' => 'required|numeric',
    ]);

    $user->name = $validatedData['name'];
    $user->email = $validatedData['email'];
    $user->phone = $validatedData['phone'];
    $user->level_id = $validatedData['level_id'];

    if ($request->hasFile('avatar')) {
        $path = $request->file('avatar')->store('public/image');
        $avatarPath = Storage::url($path);
        $user->avatar = $avatarPath;
    }

    if ($user->save()) {
        return response()->json(['message' => 'Usuario actualizado con éxito', 'data' => $user], 200);
    } else {
        return response()->json(['message' => 'Error al actualizar usuario'], 500);
    }
}
    
}
