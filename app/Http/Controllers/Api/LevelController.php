<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function list() {

        $levels = Level::where('status', '=', 1)->get();


		$list = [];

        foreach ($levels as $level) {

            $object = [
                
                'id' => $level ->id,
                'name' => $level->name,
                'code' => $level->code,
                'description'=> $level ->description,
                'created_at' => $level->created_at ? $level->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $level->updated_at ? $level->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $level = Level::where('id', '=', $id)->first();
        
        $object = [
            'id' => $level ->id,
            'name' => $level->name,
            'code' => $level->code,
            'description'=> $level ->description,
            'created_at' => $level->created_at ? $level->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $level->updated_at ? $level->updated_at->diffForHumans() : 'Fecha no disponible',
    
        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        
        $data = $request->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);

        $level = Level::create([

            'name' => $data['name'],
            'code' => $data['code'],
            'description' => $data['description'],


        ]);


        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Nivel correctamente',
            'data' => $level
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $level = Level::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'status' => 'integer'
        ]);



        $level->status = $data['status'];
        $level->save();

 

        $object = [
            'response' => 'ok',
            'message' => 'El Nivel ha sido inhabilitado correctamente.',
            'data' => $level
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $level = Level::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);



        $level->name = $data['name'];
        $level->code = $data['code'];
        $level->description = $data['description'];



        $level->save();

        $object = [
            'response' => 'ok',
            'message' => 'El Nivel ha sido modificado correctamente.',
            'data' => $level
        ];

        return response()->json($object);
    }
}
