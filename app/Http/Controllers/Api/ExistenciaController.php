<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Existencia;
use Illuminate\Http\Request;

class ExistenciaController extends Controller
{
    public function list() {

        $existencias = Existencia::where('status', '=', 1)->get();


		$list = [];

        foreach ($existencias as $existencia) {

            $object = [
                
                'id' => $existencia ->id,
                'nombre_categoria' => $existencia->cantidad_disponible,
                'dispositivos_id' => $existencia->dispositivos_id,
                'created_at' => $existencia->created_at ? $existencia->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $existencia->updated_at ? $existencia->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $existencia = Existencia::where('id', '=', $id)->first();
        
        $object = [
            'id' => $existencia ->id,
            'cantidad_disponible' => $existencia->cantidad_disponible,
            'dispositivos_id' => $existencia->dispositivos_id,
            'created_at' => $existencia->created_at ? $existencia->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $existencia->updated_at ? $existencia->updated_at->diffForHumans() : 'Fecha no disponible',
    
        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        

        $data = $request->validate([

            'cantidad_disponible' => 'required|numeric',
            'dispositivos_id' => 'required|numeric',
        ]);


        $existencia = Existencia::create([

            'cantidad_disponible' => $data['cantidad_disponible'],
            'dispositivos_id' => $data['dispositivos_id'],


        ]);


        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Nueva Existencia correctamente',
            'data' => $existencia
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $existencia = Existencia::where('id', '=', $request->id)->first();

        $data = $request->validate([
            'status' => 'integer'
        ]);


        $existencia->status = $data['status'];
        $existencia->save();

        $object = [
            'response' => 'ok',
            'message' => 'La Existencia ha sido inhabilitado correctamente.',
            'data' => $existencia
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $existencia = Existencia::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'cantidad_disponible' => 'required',
            'dispositivos_id' => 'required',
        ]);


        
        $existencia->cantidad_disponible = $data['cantidad_disponible'];
        $existencia->dispositivos_id = $data['dispositivos_id'];

        $existencia->save();

        $object = [
            'response' => 'ok',
            'message' => 'La Existencia ha sido modificada correctamente.',
            'data' => $existencia
        ];

        return response()->json($object);
    }

}
