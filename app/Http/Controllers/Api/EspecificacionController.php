<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Especificacion;
use Illuminate\Http\Request;

class EspecificacionController extends Controller
{
    public function list() {

        $especificaciones = Especificacion::where('status', '=', 1)->get();


		$list = [];

        foreach ($especificaciones as $especificacion) {

            $object = [
                
                'id' => $especificacion ->id,
                'almacenamiento' => $especificacion->almacenamiento,
                'ram' => $especificacion->ram,
                'pantalla' => $especificacion->nombre_fabricante,
                'sistema_operativo' => $especificacion->sistema_operativo,
                'bateria' => $especificacion->bateria,
                'procesador' => $especificacion->procesador,
                'color' => $especificacion->color,
                'dimensiones' => $especificacion->dimensiones,
                'camaras' => $especificacion->camaras,
                'dimensiones' => $especificacion->dispositivos_id,
                'created_at' => $especificacion->created_at ? $especificacion->created_at->diffForHumans() : 'Fecha no disponible',
                'updated_at' => $especificacion->updated_at ? $especificacion->updated_at->diffForHumans() : 'Fecha no disponible',
        
            ];

            array_push($list, $object);
        }

        return response()->json($list);
	}

    public function item($id) {

        $especificacion = Especificacion::where('id', '=', $id)->first();
        
        $object = [
            'id' => $especificacion ->id,
            'almacenamiento' => $especificacion->almacenamiento,
            'ram' => $especificacion->ram,
            'pantalla' => $especificacion->nombre_fabricante,
            'sistema_operativo' => $especificacion->sistema_operativo,
            'bateria' => $especificacion->bateria,
            'procesador' => $especificacion->procesador,
            'color' => $especificacion->color,
            'dimensiones' => $especificacion->dimensiones,
            'camaras' => $especificacion->camaras,
            'dimensiones' => $especificacion->dispositivos_id,
            'created_at' => $especificacion->created_at ? $especificacion->created_at->diffForHumans() : 'Fecha no disponible',
            'updated_at' => $especificacion->updated_at ? $especificacion->updated_at->diffForHumans() : 'Fecha no disponible',
    
    
        ];

        return response()->json($object);
    }



     public function create(Request $request) {
        
        $data = $request->validate([

            'almacenamiento'=> 'required', 
            'ram'=> 'required', 
            'pantalla'=> 'required', 
            'sistema_operativo'=> 'required', 
            'bateria'=> 'required', 
            'procesador'=> 'required', 
            'color'=> 'required', 
            'dimensiones'=> 'required', 
            'camaras'=> 'required',
            'dispositivos_id'=> 'required|numeric',
        ]);

        $especificacion = Especificacion::create([

            'almacenamiento' => $data['almacenamiento'],
            'ram' => $data['ram'],
            'pantalla' => $data['pantalla'],
            'sistema_operativo' => $data['sistema_operativo'],
            'bateria' => $data['bateria'],
            'procesador' => $data['procesador'],
            'color' => $data['color'],
            'dimensiones' => $data['dimensiones'],
            'camaras' => $data['camaras'],
            'dispositivos_id' => $data['dispositivos_id'],
          
      

        ]);


        $object = [
            'response' => 'ok',
            'message' => 'Se añadío Especificacion correctamente',
            'data' => $especificacion
        ];

        return response()->json($object);
    }




    public function deactivate(Request $request) {    

        $especificacion = Especificacion::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'status' => 'integer'
        ]);



        $especificacion->status = $data['status'];
        $especificacion->save();

 

        $object = [
            'response' => 'ok',
            'message' => 'La Especificacion ha sido inhabilitado correctamente.',
            'data' => $especificacion
        ];

        return response()->json($object);
    }


    public function update(Request $request) {    

        $especificacion = Especificacion::where('id', '=', $request->id)->first();


        $data = $request->validate([
            'almacenamiento'=> 'required', 
            'ram'=> 'required', 
            'pantalla'=> 'required', 
            'sistema_operativo'=> 'required', 
            'bateria'=> 'required', 
            'procesador'=> 'required', 
            'color'=> 'required', 
            'dimensiones'=> 'required', 
            'camaras'=> 'required',
            'dispositivos_id'=> 'required|numeric',
        ]);

        
        $especificacion->almacenamiento = $data['almacenamiento'];
        $especificacion->ram = $data['ram'];
        $especificacion->pantalla = $data['pantalla'];
        $especificacion->sistema_operativo = $data['sistema_operativo'];
        $especificacion->bateria = $data['bateria'];
        $especificacion->procesador = $data['procesador'];
        $especificacion->color = $data['color'];
        $especificacion->dimensiones = $data['dimensiones'];
        $especificacion->camaras = $data['camaras'];
        $especificacion->dispositivos_id = $data['dispositivos_id'];
      

        $especificacion->save();

        $object = [
            'response' => 'ok',
            'message' => 'La Especificacion ha sido modificada correctamente.',
            'data' => $especificacion
        ];

        return response()->json($object);
    }
}
