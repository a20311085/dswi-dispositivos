<?php

namespace App\Http\Controllers;

use App\Models\Fabricante;
use Illuminate\Http\Request;

class FabricanteController extends Controller
{
    public function index()
    {
        //->orderBy('name', 'ASC')
        $fabricante = Fabricante::where('status', '=', 1)->paginate(8);


        return view('admin.fabricantes.index', compact('fabricante'));

    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'nombre_fabricante'=> 'required', 
            'pais_origen'=> 'required', 
       
        ]);

        $fabric = new Fabricante();

     
        $fabric->nombre_fabricante = $validatedData['nombre_fabricante'];
        $fabric->pais_origen = $validatedData['pais_origen'];
     
        if($fabric->save()){
            return redirect(route('fabricante'))->with('Completado!', 'Categoria agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('fabricante'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'nombre_fabricante'=> 'required', 
            'pais_origen'=> 'required', 
        ]);

        $fabric = Fabricante::find($id);
        
        if (!$fabric) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $fabric->nombre_fabricante = $validatedData['nombre_fabricante'];
        $fabric->pais_origen = $validatedData['pais_origen'];
       

        if($fabric->save()){
            return redirect(route('fabricante'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('fabricante'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
        ]);

        $cate = Fabricante::find($id);
        // Verificar si el usuario existe
        if (!$cate) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $cate->status = $validatedData['status'];
       

        if($cate->save()){
            return redirect(route('fabricante'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('fabricante'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 
}
