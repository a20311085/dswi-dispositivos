<?php

namespace App\Http\Controllers;

use App\Models\CallSection;
use App\Models\Dispositivo;
use App\Models\HeroSection;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $herosection = HeroSection::all();
        $user = User::all();
        $callsection = CallSection::all();  
        $dispositivo = Dispositivo::where('categoria_id', '=', 1)
                          ->where('status', '=', 1)
                          ->orderBy('nombre_dispositivo', 'ASC')
                          ->get();


        return view('index' , compact('herosection','callsection','dispositivo','user'));
    }

   


    public function home()
    {
        return view('home');
    }

    public function denegado()
    {
        return view('auth.denegado');
    }

   
    
}
