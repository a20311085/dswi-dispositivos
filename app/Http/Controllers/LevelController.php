<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function index()
    {
        
      
        $levels = Level::where('status', '=', 1)->get();


        return view('admin.niveles.index',compact('levels'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'name'=> 'required',
            'code'=> 'required',
            'description'=> 'required',
           
        ]);

        $level = new Level();

     
        $level->name = $validatedData['name'];
        $level->code = $validatedData['code'];
        $level->description = $validatedData['description'];
     
        if($level->save()){
            return redirect(route('nivel'))->with('Completado!', 'Categoria agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('nivel'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'name'=> 'required',
            'code'=> 'required',
            'description'=> 'required',
       
        ]);

        $level = Level::find($id);
        
        if (!$level) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $level->name = $validatedData['name'];
        $level->code = $validatedData['code'];
        $level->description = $validatedData['description'];
     

        if($level->save()){
            return redirect(route('nivel'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('nivel'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
            'code'=> 'required', 
        ]);

        $level = Level::find($id);
        // Verificar si el usuario existe
        if (!$level) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $level->status = $validatedData['status'];
        $level->code = $validatedData['code'];

        if($level->save()){
            return redirect(route('nivel'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('nivel'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 
}
