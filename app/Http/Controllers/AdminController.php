<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Dispositivo;
use App\Models\Especificacion;
use App\Models\Existencia;
use App\Models\Fabricante;
use App\Models\User;
use App\Models\Level;
use Illuminate\Http\Request;


class AdminController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');

        $this->middleware('auth')->except(['index']);

    
    }

    
    public function menu()
    {
        $dispositivo = Dispositivo::where('status', '=', 1)->count();
        $fabricante = Fabricante::where('status', '=', 1)->count();
        $categoria = Categoria::where('status', '=', 1)->count();
        $especificacion = Especificacion::where('status', '=', 1)->count();
        $existencia = Existencia::where('status', '=', 1)->count();
        $user = User::where('status', '=', 1)->count();
        $level = Level::where('status', '=', 1)->count();


        return view('menu',compact('dispositivo','fabricante','categoria','especificacion','existencia','level','user'));
    }
}
