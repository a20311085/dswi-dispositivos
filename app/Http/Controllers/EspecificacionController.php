<?php

namespace App\Http\Controllers;

use App\Models\Dispositivo;
use App\Models\Especificacion;
use Illuminate\Http\Request;

class EspecificacionController extends Controller
{
    public function index()
    {
        //->orderBy('name', 'ASC')
        $especificacion = Especificacion::where('status', '=', 1)->paginate(8);
        $dispositivo = Dispositivo::where('status', '=', 1)->get();


        return view('admin.especificaciones.index', compact('especificacion','dispositivo'));

    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'almacenamiento'=> 'required', 
            'ram'=> 'required', 
            'pantalla'=> 'required', 
            'sistema_operativo'=> 'required', 
            'bateria'=> 'required', 
            'procesador'=> 'required', 
            'color'=> 'required', 
            'dimensiones'=> 'required', 
            'camaras'=> 'required',
            'dispositivos_id'=> 'required|numeric',
           

            
        ]);

        $espe = new Especificacion();
      
        
        $espe->almacenamiento = $validatedData['almacenamiento'];
        $espe->ram = $validatedData['ram'];
        $espe->pantalla =  $validatedData['pantalla'];
        $espe->sistema_operativo = $validatedData['sistema_operativo'];
        $espe->bateria = $validatedData['bateria'];
        $espe->procesador =  $validatedData['procesador'];
        $espe->color = $validatedData['color'];
        $espe->dimensiones =  $validatedData['dimensiones'];
        $espe->camaras = $validatedData['camaras'];
        $espe->dispositivos_id = $validatedData['dispositivos_id'];

     
         $espe->save();
         
         
         $dispositivoId = $request->input('dispositivos_id');

    // Actualizar el campo especificacion_id del dispositivo
        $dispositivo = Dispositivo::find($dispositivoId);
        if ($dispositivo) {
            $dispositivo->especificacion_id = $espe->id; // Aquí asumo que $espe->id es el ID de la nueva especificación
            $dispositivo->save();
    }
     
  
        return redirect(route('especificacion'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
  
        
    
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'almacenamiento'=> 'required', 
            'ram'=> 'required', 
            'pantalla'=> 'required', 
            'sistema_operativo'=> 'required', 
            'bateria'=> 'required', 
            'procesador'=> 'required', 
            'color'=> 'required', 
            'dimensiones'=> 'required', 
            'camaras'=> 'required',
            'dispositivos_id'=> 'required|numeric',
        ]);

        $espe = Especificacion::find($id);
        // Verificar si el usuario existe
        if (!$espe) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $espe->almacenamiento = $validatedData['almacenamiento'];
        $espe->ram = $validatedData['ram'];
        $espe->pantalla =  $validatedData['pantalla'];
        $espe->sistema_operativo = $validatedData['sistema_operativo'];
        $espe->bateria = $validatedData['bateria'];
        $espe->procesador =  $validatedData['procesador'];
        $espe->color = $validatedData['color'];
        $espe->dimensiones =  $validatedData['dimensiones'];
        $espe->camaras = $validatedData['camaras'];
        $espe->dispositivos_id = $validatedData['dispositivos_id'];

        $espe->save();

        $dispositivoId = $request->input('dispositivos_id');

           // Actualizar el campo especificacion_id del dispositivo
           $dispositivo = Dispositivo::find($dispositivoId);
           if ($dispositivo) {
               $dispositivo->especificacion_id = $espe->id; // Aquí asumo que $espe->id es el ID de la nueva especificación
         $dispositivo->save();
       }
        
     
           return redirect(route('especificacion'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
     
           
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
            'dispositivos_id'=> 'required|numeric',
            'especificacion_id'=> 'required|numeric',
        ]);

        $espe = Especificacion::find($id);
        // Verificar si el usuario existe
        if (!$espe) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $espe->status = $validatedData['status'];
       

        $espe->save();

        $dispositivoId = $request->input('dispositivos_id');

        $dispositivo = Dispositivo::find($dispositivoId);
        if ($dispositivo) {
            $dispositivo-> especificacion_id = $validatedData['especificacion_id'];
            $dispositivo->save();
    }
           
        return redirect(route('especificacion'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        

       
    } 

}
