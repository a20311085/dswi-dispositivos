<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index()
    {
        //->orderBy('name', 'ASC')
        $categoria = Categoria::where('status', '=', 1)->paginate(8);


        return view('admin.categorias.index', compact('categoria'));

    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'nombre_categoria'=> 'required', 
            'descripcion_categoria'=> 'required', 
           
        ]);

        $cate = new Categoria();

     
        $cate->nombre_categoria = $validatedData['nombre_categoria'];
        $cate->descripcion_categoria = $validatedData['descripcion_categoria'];
     
        if($cate->save()){
            return redirect(route('categoria'))->with('Completado!', 'Categoria agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('categoria'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'nombre_categoria'=> 'required', 
            'descripcion_categoria'=> 'required', 
        ]);

        $cate = Categoria::find($id);
        
        if (!$cate) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $cate->nombre_categoria = $validatedData['nombre_categoria'];
        $cate->descripcion_categoria = $validatedData['descripcion_categoria'];
       

        if($cate->save()){
            return redirect(route('categoria'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('categoria'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
        ]);

        $cate = Categoria::find($id);
        // Verificar si el usuario existe
        if (!$cate) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $cate->status = $validatedData['status'];
       

        if($cate->save()){
            return redirect(route('categoria'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('categoria'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 
}
