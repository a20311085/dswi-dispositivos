<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        
      
        $users = User::where('status', '=', 1)->paginate(8);
        $level = Level::where('status', '=', 1)->get();


        return view('admin.usuarios.index',compact('users','level'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([

    
            'name'=> 'required',
            'avatar'=> 'nullable|mimes:jpeg,png,jpg,gif|max:2048',
            'email'=> 'required|unique:users',
            'phone'=> 'required',
            'level_id'=> 'required|numeric',
           
        ]);

        $user = new User();

     
        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];
        $user->phone = $validatedData['phone'];
        $user->level_id = $validatedData['level_id'];

        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar')->store('public/image'); // Almacenar el archivo en la carpeta "public/image"
            $avatarPath = Storage::url($path); // Obtener la URL completa utilizando Storage
            $user->avatar = $avatarPath; // Asignar la ruta completa al campo de avatar
        }
     
        if($user->save()){
            return redirect(route('usuario'))->with('Completado!', 'Categoria agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('usuario'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'avatar'=> 'nullable|mimes:jpeg,png,jpg,gif|max:2048', // Puedes hacer el campo 'avatar' opcional aquí si no es necesario actualizarlo en cada actualización.
            'email' => 'required|unique:users,email,' . $id,
            'phone' => 'required',
            'level_id' => 'required|numeric',
        ]);
    
        $user = User::find($id);
        
        if (!$user) {
            return back()->with('error', 'Usuario no encontrado');
        }
    
        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];
        $user->phone = $validatedData['phone'];
        $user->level_id = $validatedData['level_id'];
    
        // Verificar si se cargó un archivo de avatar
        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar')->store('public/image'); // Almacenar el archivo en la carpeta "public/image"
            $avatarPath = Storage::url($path); // Obtener la URL completa utilizando Storage
            $user->avatar = $avatarPath; // Asignar la ruta completa al campo de avatar
        }
    
        if ($user->save()) {
            return redirect(route('usuario'))->with('Completado!', 'Usuario actualizado correctamente');
        } else {
            return redirect(route('usuario'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    }

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
            'level_id'=> 'required', 
        ]);

        $user = User::find($id);
        // Verificar si el usuario existe
        if (!$user) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $user->status = $validatedData['status'];
        $user->level_id = $validatedData['level_id'];

        if($user->save()){
            return redirect(route('usuario'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('usuario'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 
}

