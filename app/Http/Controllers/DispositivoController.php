<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Dispositivo;
use App\Models\Especificacion;
use App\Models\Fabricante;
use Illuminate\Http\Request;

class DispositivoController extends Controller
{
    //

    public function index()
    {
        //->orderBy('name', 'ASC')

        
        $dispositivo = Dispositivo::where('status', '=', 1)->paginate(10);
        $categoria = Categoria::where('status', '=', 1)->get();
        $fabricante = Fabricante::where('status', '=', 1)->get();
        $especificaciones = Especificacion::where('status', '=', 1)->get();

        return view('admin.dispositivos.index', compact('dispositivo', 'categoria', 'fabricante', 'especificaciones'));

    }

  
    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'nombre_dispositivo'=> 'required', 
            'precio'=> 'required', 
            'image'=> 'required',
            'fabricante_id' => 'required|numeric', 
            'categoria_id' => 'required|numeric', 
        ]);

        $dispo = new Dispositivo();
        
        
        $dispo->nombre_dispositivo = $validatedData['nombre_dispositivo'];
        $dispo->precio = $validatedData['precio'];
        $dispo->image =  $validatedData['image'];
        $dispo->fabricante_id = $validatedData['fabricante_id'];
        $dispo->categoria_id = $validatedData['categoria_id'];
       

        if($dispo->save()){
            return redirect(route('dispositivos'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('dispositivos'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'nombre_dispositivo'=> 'required', 
            'precio'=> 'required', 
            'image'=> 'required',
            'fabricante_id' => 'required|numeric', 
            'categoria_id' => 'required|numeric', 
        ]);

        $dispo = Dispositivo::find($id);
        // Verificar si el usuario existe
        if (!$dispo) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $dispo->nombre_dispositivo = $validatedData['nombre_dispositivo'];
        $dispo->precio = $validatedData['precio'];
        $dispo->image =  $validatedData['image'];
        $dispo->fabricante_id = $validatedData['fabricante_id'];
        $dispo->categoria_id = $validatedData['categoria_id'];
       

        if($dispo->save()){
            return redirect(route('dispositivos'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('dispositivos'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
        ]);

        $dispo = Dispositivo::find($id);
        // Verificar si el usuario existe
        if (!$dispo) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $dispo->status = $validatedData['status'];
       

        if($dispo->save()){
            return redirect(route('dispositivos'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        } else{
            return redirect(route('dispositivos'))->with('error', 'Algo salió mal. Por favor, vuelva a intentarlo.');
        }
    } 




}
