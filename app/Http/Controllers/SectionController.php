<?php

namespace App\Http\Controllers;
use App\Models\CallSection;
use App\Models\HeroSection;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function callsection() {

        $callsection = CallSection::all();


        return view('admin.sections.call', compact('callsection'));
    }

    public function herosection() {

        $herosection = HeroSection::all();


        return view('admin.sections.hero', compact('herosection'));
    }

  
    public function updatehero(Request $request) {

        $hero = HeroSection::firstOrFail();

        $hero->update([
            'title' => $request->input('title'),
            'paragraph' => $request->input('paragraph'),
            'image' => $request->input('image'),
        ]);
        
        return redirect(route('index'))->with('Completado!', 'Section Modificada correctamente');

   
    }

    public function updatecall(Request $request) {

        $call = callSection::firstOrFail();

        $call->update([
            'title' => $request->input('title'),
            'paragraph' => $request->input('paragraph'),
            'image' => $request->input('image'),
        ]);
        
        return redirect(route('index'))->with('Completado!', 'Section Modificada correctamente');

    
   
    }

}
