<?php

namespace App\Http\Controllers;

use App\Models\Dispositivo;
use App\Models\Existencia;
use Illuminate\Http\Request;

class ExistenciaController extends Controller
{
    public function index()
    {
        //->orderBy('name', 'ASC')
        $existencia = Existencia::where('status', '=', 1)->paginate(8);
        $dispositivo = Dispositivo::where('status', '=', 1)->get();


        return view('admin.existencias.index', compact('existencia','dispositivo'));

    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'cantidad_disponible'=> 'required', 
            'dispositivos_id'=> 'required|numeric',
           
        ]);

        $exite = new Existencia();
      
        
        $exite->cantidad_disponible = $validatedData['cantidad_disponible'];
        $exite->dispositivos_id = $validatedData['dispositivos_id'];

     
         $exite->save();
         
         
         $dispositivoId = $request->input('dispositivos_id');

    // Actualizar el campo especificacion_id del dispositivo
        $dispositivo = Dispositivo::find($dispositivoId);
        if ($dispositivo) {
            $dispositivo->existencia_id = $exite->id; // Aquí asumo que $espe->id es el ID de la nueva especificación
            $dispositivo->save();
    }
        return redirect(route('existencia'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
       
    } 

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'cantidad_disponible'=> 'required', 
            'dispositivos_id'=> 'required|numeric',
        ]);

        $exite = Existencia::find($id);
        // Verificar si el usuario existe
        if (!$exite) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $exite->cantidad_disponible = $validatedData['cantidad_disponible'];
        $exite->dispositivos_id = $validatedData['dispositivos_id'];

        $exite->save();

        $dispositivoId = $request->input('dispositivos_id');

           // Actualizar el campo especificacion_id del dispositivo
           $dispositivo = Dispositivo::find($dispositivoId);
           if ($dispositivo) {
               $dispositivo->existencia_id = $exite->id; // Aquí asumo que $espe->id es el ID de la nueva especificación
         $dispositivo->save();
       }
        
     
           return redirect(route('existencia'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
     
           
    } 

    public function delete(Request $request, $id)
    {
        $validatedData = $request->validate([

            'status'=> 'required', 
            'dispositivos_id'=> 'required|numeric',
            'existencia_id'=> 'required|numeric',
        ]);

        $exite = Existencia::find($id);
        // Verificar si el usuario existe
        if (!$exite) {
            return back()->with('error', 'Usuario no encontrado');
        }

        $exite->status = $validatedData['status'];
       

        $exite->save();

        $dispositivoId = $request->input('dispositivos_id');

        $dispositivo = Dispositivo::find($dispositivoId);
        if ($dispositivo) {
            $dispositivo-> existencia_id = $validatedData['existencia_id'];
            $dispositivo->save();
    }
           
        return redirect(route('existencia'))->with('Completado!', 'Dispositivo agregado, te recomendamos agregar las espicificaciones y existencia');
        

       
    } 
}
