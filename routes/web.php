<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoriaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DispositivoController;
use App\Http\Controllers\EspecificacionController;
use App\Http\Controllers\ExistenciaController;
use App\Http\Controllers\FabricanteController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/home', [HomeController::class, 'home'])->name('home');
Route::get('/', [HomeController::class, 'app'])->name('app');


Route::prefix('/')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/denegado', [HomeController::class, 'denegado'])->name('denegado');
  
   
});


Route::prefix('/admin')->group(function () {
    Route::get('/', [AdminController::class, 'menu'])->name('menu')->middleware('level-in');
   
   
    Route::get('/dispositivos', [DispositivoController::class, 'index'])->name('dispositivos')->middleware('level-in');
    Route::post('/dispositivos/create', [DispositivoController::class, 'store'])->name('dispositivo.create')->middleware('level-mo');
    Route::put('/dispositivos/update/{id}', [DispositivoController::class, 'update'])->name('dispositivo.update')->middleware('level-mo');
    Route::put('/dispositivos/delete/{id}', [DispositivoController::class, 'delete'])->name('dispositivo.delete')->middleware('level-mo');




    Route::get('/fabricantes', [FabricanteController::class, 'index'])->name('fabricante')->middleware('level-mo')->middleware('level-mo');
    Route::post('/fabricantes/create', [FabricanteController::class, 'store'])->name('fabricante.create')->middleware('level-mo');
    Route::put('/fabricantes/update/{id}', [FabricanteController::class, 'update'])->name('fabricante.update')->middleware('level-mo');
    Route::put('/fabricantes/delete/{id}', [FabricanteController::class, 'delete'])->name('fabricante.delete')->middleware('level-mo');


    Route::get('/categorias', [CategoriaController::class, 'index'])->name('categoria')->middleware('level-mo');
    Route::post('/categorias/create', [CategoriaController::class, 'store'])->name('categoria.create')->middleware('level-mo');
    Route::put('/categorias/update/{id}', [CategoriaController::class, 'update'])->name('categoria.update')->middleware('level-mo');
    Route::put('/categorias/delete/{id}', [CategoriaController::class, 'delete'])->name('categoria.delete')->middleware('level-mo');

    Route::get('/especificaciones', [EspecificacionController::class, 'index'])->name('especificacion')->middleware('level-ad');
    Route::post('/especificaciones/create', [EspecificacionController::class, 'store'])->name('especificacion.create')->middleware('level-ad');
    Route::put('/especificaciones/update/{id}', [EspecificacionController::class, 'update'])->name('especificacion.update')->middleware('level-ad');
    Route::put('/especificaciones/delete/{id}', [EspecificacionController::class, 'delete'])->name('especificacion.delete')->middleware('level-ad');

    Route::get('/existencias', [ExistenciaController::class, 'index'])->name('existencia')->middleware('level-ad');
    Route::post('/existencias/create', [ExistenciaController::class, 'store'])->name('existencia.create')->middleware('level-ad');
    Route::put('/existencias/update/{id}', [ExistenciaController::class, 'update'])->name('existencia.update')->middleware('level-ad');
    Route::put('/existencias/delete/{id}', [ExistenciaController::class, 'delete'])->name('existencia.delete')->middleware('level-ad');

    Route::get('/niveles', [LevelController::class, 'index'])->name('nivel')->middleware('level-ad');
    Route::post('/niveles/create', [LevelController::class, 'store'])->name('nivel.create')->middleware('level-ad');
    Route::put('/niveles/update/{id}', [LevelController::class, 'update'])->name('nivel.update')->middleware('level-su');
    Route::put('/niveles/delete/{id}', [LevelController::class, 'delete'])->name('nivel.delete')->middleware('level-su');


    Route::get('/usuarios', [UserController::class, 'index'])->name('usuario')->middleware('level-su');
    Route::post('/usuarios/create', [UserController::class, 'store'])->name('usuario.create')->middleware('level-su');
    Route::put('/usuarios/update/{id}', [UserController::class, 'update'])->name('usuario.update')->middleware('level-su');
    Route::put('/usuarios/delete/{id}', [UserController::class, 'delete'])->name('usuario.delete')->middleware('level-su');


    Route::get('/hero-section', [SectionController::class, 'herosection'])->name('herosection')->middleware('level-in');
    Route::put('/hero-section', [SectionController::class, 'updatehero'])->name('sections.update')->middleware('level-in');

    Route::get('/call-section', [SectionController::class, 'callsection'])->name('callsection')->middleware('level-in');
    Route::put('/call-section', [SectionController::class, 'updatecall'])->name('callsections.update')->middleware('level-in');
});










Auth::routes();


// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('home', [HomeController::class, 'index']);

// Route::get('/dispositivos', [DispositivoController::class, 'index'])->name('dispositivos');

