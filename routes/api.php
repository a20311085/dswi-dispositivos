<?php

use App\Http\Controllers\Api\CategoriaController;
use App\Http\Controllers\Api\DispositivoController;
use App\Http\Controllers\Api\EspecificacionController;
use App\Http\Controllers\Api\ExistenciaController;
use App\Http\Controllers\Api\FabricanteController;
use App\Http\Controllers\Api\LevelController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/




Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/categorias', [CategoriaController::class, 'list']);
Route::post('/categorias/add', [CategoriaController::class, 'create']);
Route::post('/categorias/deactivate', [CategoriaController::class, 'deactivate']);
Route::post('/categorias/update', [CategoriaController::class, 'update']);
Route::get('/categorias/{id}', [CategoriaController::class, 'item']);


Route::get('/dispositivos', [DispositivoController::class, 'list']);
Route::post('/dispositivos/add', [DispositivoController::class, 'create']);
Route::post('/dispositivos/deactivate', [DispositivoController::class, 'deactivate']);
Route::post('/dispositivos/update', [DispositivoController::class, 'update']);
Route::get('/dispositivos/{id}', [DispositivoController::class, 'item']);


Route::get('/fabricantes', [FabricanteController::class, 'list']);
Route::post('/fabricantes/add', [FabricanteController::class, 'create']);
Route::post('/fabricantes/deactivate', [FabricanteController::class, 'deactivate']);
Route::post('/fabricantes/update', [FabricanteController::class, 'update']);
Route::get('/fabricantes/{id}', [FabricanteController::class, 'item']);

Route::get('/existencias', [ExistenciaController::class, 'list']);
Route::post('/existencias/add', [ExistenciaController::class, 'create']);
Route::post('/existencias/deactivate', [ExistenciaController::class, 'deactivate']);
Route::post('/existencias/update', [ExistenciaController::class, 'update']);
Route::get('/existencias/{id}', [ExistenciaController::class, 'item']);

Route::get('/especificaciones', [EspecificacionController::class, 'list']);
Route::post('/especificaciones/add', [EspecificacionController::class, 'create']);
Route::post('/especificaciones/deactivate', [EspecificacionController::class, 'deactivate']);
Route::post('/especificaciones/update', [EspecificacionController::class, 'update']);
Route::get('/especificaciones/{id}', [EspecificacionController::class, 'item']);

Route::get('/users', [UserController::class, 'list']);
Route::post('/users/add', [UserController::class, 'create']);
Route::post('/users/deactivate', [UserController::class, 'deactivate']);
Route::post('/users/update', [UserController::class, 'update']);
Route::get('/users/{id}', [UserController::class, 'item']);

Route::get('/levels', [LevelController::class, 'list']);
Route::post('/levels/add', [LevelController::class, 'create']);
Route::post('/levels/deactivate', [LevelController::class, 'deactivate']);
Route::post('/levels/update', [LevelController::class, 'update']);
Route::get('/levels/{id}', [LevelController::class, 'item']);
