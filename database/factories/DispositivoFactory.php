<?php

namespace Database\Factories;

use App\Models\Dispositivo;
use Illuminate\Database\Eloquent\Factories\Factory;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Dispositivo>
 */
class DispositivoFactory extends Factory
{   

   
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            // 'nombre_dispositivo' =>$this->faker->name('celular samsung','tablet xiomi','smartwatch pixel','iphone 12 pro'), 
            // 'precio'=>$this->faker->money_format(), 
            // 'id_fabricante'=>$this->faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]), 
            // 'id_categoria'=>$this->faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]), 
            // 'id_especificaciones'=>$this->faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])
      
            'nombre_dispositivo'=>fake()->randomElement(['samsung a20','Redmi note 9 pro','tablet xiomi','smartwatch pixel','iphone 12 pro']),
            'precio' => fake()->randomFloat(2, 100, 1000),
            'id_fabricante' =>fake() ->numberBetween(1, 20),
            'id_categoria' =>fake()->numberBetween(1, 20),
            'id_especificaciones' =>fake()->numberBetween(1, 20)

            
        ];
    }
}
