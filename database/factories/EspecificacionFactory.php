<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Especificacion>
 */
class EspecificacionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'almacenamiento' => fake()->randomElement(['64GB', '128GB', '256GB']),
            'ram' =>fake()->randomElement(['4GB', '6GB', '8GB']),
            'pantalla' => fake()->randomElement(['5.5"', '6.2"', '6.7"']),
            'sistema_operativo' => fake()->randomElement(['Android', 'iOS']),
            'bateria' => fake()->randomElement(['3000mAh', '4000mAh', '5000mAh']),
            'procesador' =>fake()->randomElement(['Snapdragon 865', 'Apple A14 Bionic', 'Exynos 990']),
            'color' => fake()->colorName,
            'dimensiones' => fake()->randomElement(['150 x 70 x 8mm', '160 x 75 x 9mm', '155 x 72 x 7mm']),
            'camaras' => fake()->randomElement(['12MP+8MP', '48MP+16MP', '64MP+32MP']),
        ];
    }
}
