<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Existencia;
use Carbon\Carbon;

class ExistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $existencia = new Existencia();


        $existencia =[

            [
                //1
                'cantidad_disponible'=> 100,
                'dispositivos_id'=>1,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //2
                'cantidad_disponible'=> 121,
                'dispositivos_id'=>2,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //3
                'cantidad_disponible'=> 30,
                'dispositivos_id'=>3,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //4
                'cantidad_disponible'=> 100,
                'dispositivos_id'=>4,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //5
                'cantidad_disponible'=> 220,
                'dispositivos_id'=>5,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //6
                'cantidad_disponible'=> 130,
                'dispositivos_id'=>6,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //7
                'cantidad_disponible'=> 80,
                'dispositivos_id'=>7,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //8
                'cantidad_disponible'=> 90,
                'dispositivos_id'=>8,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //9
                'cantidad_disponible'=> 77,
                'dispositivos_id'=>9,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //10
                'cantidad_disponible'=> 65,
                'dispositivos_id'=>10,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //11
                'cantidad_disponible'=> 100,
                'dispositivos_id'=>11,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //12
                'cantidad_disponible'=> 36,
                'dispositivos_id'=>12,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //13
                'cantidad_disponible'=> 130, 
                'dispositivos_id'=>13,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //14
                'cantidad_disponible'=> 30, 
                'dispositivos_id'=>14,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //15
                'cantidad_disponible'=> 70,
                'dispositivos_id'=>15,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //16
                'cantidad_disponible'=> 40,
                'dispositivos_id'=>16,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //17
                'cantidad_disponible'=> 50,
                'dispositivos_id'=>17,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //18
                'cantidad_disponible'=> 120,
                'dispositivos_id'=>18,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //19
                'cantidad_disponible'=> 120,
                'dispositivos_id'=>19,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //20
                'cantidad_disponible'=> 10,
                'dispositivos_id'=>20,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            
        ];
        DB::table('existencias')-> insert($existencia);
    }
}
