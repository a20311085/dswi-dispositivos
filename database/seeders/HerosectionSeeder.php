<?php

namespace Database\Seeders;

use App\Models\HeroSection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class HerosectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $herosection = new HeroSection();
        $herosection =[

            [
                //1
                'title'=>'Hero Section',
                'paragraph'=>' Lorem ipsum dolor sit amet 
                consectetur adipisicing elit. Natus veniam enim deleniti 
                similique neque magni velit cumque assumenda nam adipisci. 
                Aliquid dolor totam quae vero sunt ipsa mollitia minima autem!
                Lorem ipsum dolor sit amet 
                consectetur adipisicing elit. Natus veniam enim deleniti 
                similique neque magni velit cumque assumenda nam adipisci. 
                Aliquid dolor totam quae vero sunt ipsa mollitia minima autem!
                ',
                'image'=>'https://http2.mlstatic.com/D_NQ_NP_808543-MLM53368069833_012023-O.webp',
            ],

         
        ];
        DB::table('herosections')-> insert($herosection);
    }
}
