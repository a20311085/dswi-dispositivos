<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**php artisan make:seeder UserSeeder

     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Dispositivo::->create();
        // Especificacion::factory(20)->create();
        // Categoria::factory(20)->create();
        // Existencia::factory(20)->create();
        // Fabricante::factory(20)->create();
       
        $this->call(DispositivoSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(FabricanteSeeder::class);
        $this->call(ExistenciaSeeder::class);
        $this->call(EspecificacionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(CallsectioniaSeeder::class);
        $this->call(HerosectionSeeder::class);
       

      
    }
}
