<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Dispositivo;
use Carbon\Carbon;

class DispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dispositivo = new Dispositivo();


        $dispositivo =[

            [
                //1
                'nombre_dispositivo' => 'Xiaomi Redmi note 9',
                'precio' => 5009.00,
                'image'=> 'https://cdn1.coppel.com/images/catalog/pm/2265893-1.jpg',
                'fabricante_id' => 1, 
                'categoria_id' => 1, 
                'especificacion_id' => 1, 
                'existencia_id' => 1, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],

            [
                //2
                'nombre_dispositivo' => 'Laptop lenovo',
                'precio' => 9009.00,
                'image'=> 'https://m.media-amazon.com/images/I/61ysOnCgocL._AC_UF894,1000_QL80_.jpg',
                'fabricante_id' => 2, 
                'categoria_id' => 2, 
                'especificacion_id' => 2, 
                'existencia_id' => 2, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //3
                'nombre_dispositivo' => 'Iphone 14 pro',
                'precio' => 5009.00,
                'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUr31c_oNaX74gEOUfY-gfoszTtfGU-_yLJzdM31QREdOgkgS1Y3AKCvsJNohoAlzyhTc&usqp=CAU',
                'fabricante_id' => 3, 
                'categoria_id' => 1, 
                'especificacion_id' =>3, 
                'existencia_id' => 3, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //4
                'nombre_dispositivo' => 'Xbox serie X',
                'precio' => 14999.00,
                'image'=> 'https://res.cloudinary.com/grover/image/upload/f_webp,q_auto/b_white,c_pad,dpr_2.0,h_500,w_520/f_auto,q_auto/v1685471570/jb0wpqpwcizglwxsqvrt.png',
                'fabricante_id' => 4, 
                'categoria_id' => 3, 
                'especificacion_id' => 4, 
                'existencia_id' => 4, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //5
                'nombre_dispositivo' => 'PlayStation 5',
                'precio' => 14999.00,
                'image'=> 'https://m.media-amazon.com/images/I/41p8FiLGtrL._AC_UF1000,1000_QL80_.jpg',
                'fabricante_id' => 5, 
                'categoria_id' => 3, 
                'especificacion_id' => 5, 
                'existencia_id' => 5, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //6
                'nombre_dispositivo' => 'Nintendo Switch',
                'precio' => 5504.00,
                'image'=> 'https://http2.mlstatic.com/D_NQ_NP_969606-MLA47920420604_102021-O.webp',
                'fabricante_id' => 6, 
                'categoria_id' => 3, 
                'especificacion_id' => 6, 
                'existencia_id' => 6, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //7
                'nombre_dispositivo' => 'Samsung Galaxi 20s',
                'precio' => 24202.00,
                'image'=> 'https://cell-shop.com.mx/wp-content/uploads/2020/11/samsung-galaxy-s20-dual-sim-128gb-9.jpg',
                'fabricante_id' => 7, 
                'categoria_id' => 1, 
                'especificacion_id' => 7, 
                'existencia_id' => 7, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //8
                'nombre_dispositivo' => 'Moto g8 plus',
                'precio' => 7009.00,
                'image'=> 'https://i.blogs.es/f97289/g8plus/450_1000.webp',
                'fabricante_id' => 8, 
                'categoria_id' => 1, 
                'especificacion_id' => 8, 
                'existencia_id' => 8, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //9
                'nombre_dispositivo' => 'Super light pro X',
                'precio' => 3009.00,
                'image'=> 'https://ddtech.mx/assets/uploads/8555450fc873cb267e06b7c1a8f8633d.jpg',
                'fabricante_id' => 9, 
                'categoria_id' => 4, 
                'especificacion_id' => 9, 
                'existencia_id' => 9, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //10
                'nombre_dispositivo' => 'SmartTV sony',
                'precio' => 5009.00,
                'image'=> 'https://m.media-amazon.com/images/I/71GUJVSDv3L.jpg',
                'fabricante_id' => 5, 
                'categoria_id' => 5, 
                'especificacion_id' => 10, 
                'existencia_id' => 10, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //11
                'nombre_dispositivo' => 'iPad pro',
                'precio' => 22009.00,
                'image'=> 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIREhISEBIVFhIWGBcSFRUWFRcXGBYTFRgWFhUVFRcYHyggGBolHRUVITIhJSkrLi4uGB8zOzMtNygtLisBCgoKDg0OGxAQGy0mICUtLSstLisvLS0tKystLS0tLSstLS0tLS0uLS0tLS0tLS0tMi0rLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABAUGBwIDCAH/xABLEAABAwIDAggICgcIAwEAAAABAAIDBBEFITESQQYTIjJRYXGBM1JzkaGywdEHCBQjQmKCk7HwFyRTcpLC4hY0Q1RjorPhFYPxJf/EABoBAQADAQEBAAAAAAAAAAAAAAABAwQFAgb/xAA2EQACAQIDBQcCBAYDAAAAAAAAAQIDESExQQQSUWHwEyJxgZGhscHRBTJC4RQjYnKS8aKywv/aAAwDAQACEQMRAD8A3iiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIuuaXZF1QYjwmghdsyzsa7XZvmO0DTvQGRosSbw0pLj9ZZ3kgenJXTKwubtteC05ghAWaLTHDn4WZqaZ1PRhjnsyfI8Eta7xWtFtojeb5daxP9MeLePD9z/UgPSaLzaPhkxbx4Puf6l8Pwx4t48P3P8AUgPSaLzX+mLFv2kP3I96fpixb9pD9yPegPSiLzV+mPFv2kP3I96+t+GLFt8kP3I96A9KIvNZ+GLFv2kP3I96+fpjxb9pD9yPeougelUXmsfDHi37SH7ke9cv0y4t48H3P9SXQPSSLzb+mTFvHg+5/qXE/DFi3jw/df1KQelEXnCm+GXFGOBdxD25XaY3C46iHZHrW6uAvC2LFKYTRtLHtOxLGTcseM9d7SMwUBkqIiAIiIAiIgCIiAIiICFiXN7AT5l5S4SVLpKqoc4ku42QNdfmkPcF6txBtwQNS1wHevJ3CankgqZ2yNc0mSRzScgQ57nXz1yI7NCpQLbBMbkhMjmCN20x0REjdoWfbMDccltb4LqpzsONyTZzgL9Tntt/tHmXn2OpN7NOulhe53Cy3/8ABjQSQYYOOaWucXO2TqAXOcL9dnBQDReMvJmqDvMsp873KruelXFYD8omAtfjZdf33L6IGnnxntbY/hmtVDZ+1je9nfg/lEq6xSKgSdZXIOPSVbf+IY7mSAHofyT/ALrLqn4P1DBcMJb0tzCmWxVljFX8Mf3PaqxWD+3yRY5dx0XewOaWlpvbm30IOrHdudj3dkJ4LTZwIO/L8V2wzW6xvCrjOztLM0Rksi+icJI9kdHJJ14t3JLT1tNr/uA/SVQ64LvJk+bL2KXTS2G003F7m57jftGR6bA6tz4YkwXuNCxxHeP/AKtcXatCRbX/AJmxy4x+Hl1xudcLb7Ddw5RvplpfqvbuusupI2U8W3KzauWuMZyMsjRtRUxvoxoPGynS7g032VRYNE1u1LJYMZbUXuRoLb875aHIHnXUPFMUdM6+ezo1t72aTtEE7yTynHVxNzuC1b6hBJvx59eeOOdjlVIyqS3Y5EnEsQMpe57r8YdqR4FjKRoyMfRiblbptc7rVc9Re18gNGDd7+1R3yedcLFY6m14bsFhrz8eS4ZavebbNEKKjghJMT1Do9/SuvbPSV9LVxWKU3J3ZLVjshJuM1ur4uUh4yvbfLZgdbrvKL/gtJw6hbo+Ll4au/ch9aVeDxJG9EREPIREQBERAEREAREQEWr1HYVW1GGwyHafG0u3m2Z7SNVZ1e7v9ii3QENuEQDSJvpXzFQBGQBYDcFMuoWMn5sqQeYXz8XWSuLQ60s3JNs7ueN/ariLGKc8+l/hsPwcqd0IfWSsLi0GaUXABI5T9AVes4NxO/xJj3MC6n4ek6bw1+iNmzbJtFaLlSdl4k+lxHDiOU2dnftDzEOU6n/8WfB1j4j1xSN85i2QodNwKhfqZT2vb7GhXVFwCoTzg9x6ONN/M0krpOW6sb+t/o0Wy2TaI4OUX5N/+Trkw+nkFvllHKOiawI7Dsh3ncqmt4BxyZxbIPTDURSD+Bztr0rNaPgJhw0pi4/WkkP+0kH0KyqODWF0zNuenpYmdMu/sDiSfMqKtaEu7KN/Gxkez7udl4Nr2/Y0zV8DauE3jaXj90sNuu/J8zlWSxPa0NkY5hBu0OBHJdfaAO8Xstw1uNYfCL01BDptB8kDYwWm9ntjttlpIsHO2Gm9gSclrThLwnlrTKy7WwsbtBjGtY0u2mtF9kZgbVxfoCwThDOKatjbTDF+19SFWdNOLd97u+rSWi1s8s0iiqpnPAYwHYbp9Y73FdApndB/BdAcb6nzrm2peLWJzXiHZ1Mal/Y8uTh3Y2O35M7oAXW6B3UrCldK8Xbn1Xt+K4OrDezrg9vvC6H8Lsqim7q/FfXI89rN5MrHRlcNlWTpgfG8wP4FRpD1+cELLW2WEcYv4+5G89SPEMwtz/Fz8PXeTi9aRacZqMwtyfFy8NXeTh9aRc6pGzBvREReAEREAREQBERAEREBFrvo9/sUQlSq76Pf7FCJQHK6g4yfmypd1Bxg/NlSDzFVy7FXK69rTvJPQONN/RdbDp6doPPc4/VDWj2la1x3w1T5WX/kcsuwvhCwU8bnEAgbB1uXNy3a3yPeul+H14Q3lN4Z69cDsfhPe3qd7fq0XLN3tpkrma0cN9GgDpfc/j7lPqMcpaVt5pS/qbZre8rBaR1dWm1O0sZ9KWTktHYPyepWDo6DD7GQGtrTk1pG0wOOVms6Orf0BbJ7RBq8Vh6L1xZtrwhe13J8rpLxbfxurhcuxwoxCqY59JHFRUo51VLyGgb9lzs3n90DtVLDsuIniMkt3bDa6oZtyzSb48OpnckH/UffZzJIspddTTSPikxUGapfnSYXGbN6nz2yYwWzvuGZyIFfj+IlvGbUglm8BLJHyYwdfkFF4kTRnI8Z2HSQBj7WU3ux9urvz9DiVpaL26u/FmOcKsTsCxh1Jc47ZkLnG4LnSHOV2RBkOtiGhrRZ2NUA5Eh8azfTf2LhiM+24m/mFh0ZDcMhYbgGhdtNzGjpPsUJpuS0jCXq1b6ma3fgv6o+zuRJBn519a257Mlyl5357fYpOEU+28ecqNmpObUVqyK7tJ8i7oYC1jWsF3vIa0dZ9m/uXRXuZI/Yaf1enBaXgC8jyeUR0lzsgOgKVPIWt5HhJQY4s7bEQyklvuvbZB6nFRH7LGMtoPBN8Y6GocOjc0dGa7VdqTUV+WPvbTw1fHBXxMUW97DPTr29eBTzt2CBI23YdOo9a+CNrua/uKsIaQvJ2sydeodJO5VVRGGuIadpu51rA9i5tWLo2lJXT8n5tWv7rThfYm3g8zk+ItIvbtHYtw/Fx8NXeTh9aRaZDruA6Fub4uPhq7ycPrSLmV3Fze7kSb0REVICIiAIiIAiIgCIiAhYh9Hv9igkqbiR5vf7FXkoDldQcYPzZUolQcXPzZUg8w454ap8rL/yOUvgdVxR1A49oLHiwJ3O+iT1ajvCiY54ap8rL/yOVQphLdkpcOJZSqOnNTWnT9VgbWdi1VXOFPQMs3Ta5rWt0u0aW6/xVvQ0seHSCCjYKvF5NZHC7IARm4k82wPbbWwIace4G8JaiWGOgo42sqXkh01wLxjMvuea4C93Z5DLPTIpCKT/APOws8ZWy/3iqOrQcybnmgC5A3DlHdfXOpKpjfxei5JHWq1e3itzLgvl/vi+NkdNY8U5nhinL6lzdvEsSOZijyvDCdQ45NDRne2mWxguM1YIAa3i2bGzFF+yp9RfpkkObj19GQucRlhDTDEb0dOduV51q6rPMnewbh0XOZcsSxGRznEvN3k7b+081vd0da9U7U4Ofp8e7v4pSayiZHDC5Wym5UumdymBQnqTQH5xqohPdpy5mWKvWj4nybU+bz/kq+wiANjJcdkEFznb2xt5xHXoB1kKmgh25A3rz7PzdZNsDQ3DG2c6wubjmMAOpF723udnkw26+xLs4Op5L6vyXyZdod52XE6To6WVtg6zBGOhoGxTN+qBYvPYNXFRXAuLpJCL6udub0AD0ABSJCXOBsR9FjW3cQNdll+cdS5x1JJUeSZoIaGiWQc2MZxsP1iPCO9HbotcUo2cs9Fn+7fgvQ8qPZf3P2XBfV+lwYw5m1IeKp+vnynqG/8AAKNPI6oAigi2YwbjeS7QFzvR0LNuD3wZVlYRNWO4uPW8lwbfVZlYeYdC2LhmH4bhoAjaJJR9I5uv1eL2gd5WerUU24u7vmo2bfKUsorkru5mlVjDHX2X1fj8HmwRlr9lwIcCQQRYgi4II3Fbn+Lj4au8nD60iwj4T6cfLjOxmw2cbdsueBZ+W6+R7XFZt8XHw1d5OH1pFwKlOVOTjLNG2nNTipLU3qiIvB7CIiAIiIAiIgCIiAr8VPN7/Yq0lWGLnmd/sVWSgORKhYseQVJJULFjyCpB5oxzw1T5WX/kcqhW2N+GqfKy+u5VKgHfTzOY5r2OLXNNw4GxBG8LMcG4Q/q7qZjdipqH7MlQ93OjcRdoJ5tzfa6bAb7DCQVzNzuXuL4l1KtOm+7k80ZpibmN5DfAU4uf9SU6E9ZPK7AFjM1za+r+Wez6K+R1EgbxZN2X2tk210OeumS5SybW0bWJ3dAVtarvpWX+8sOSSSXm9TpTrU6kcFbk/v8A6zbsiC9d+Gn5wd66ZQu2iPLFtTcDtIICpT0OcsKqb4lzgMHOkyzybfTLUnq/G1t6tKh4aAHGzRoDe7ydXG2ZJ8/pcZuH4QA1rJJWQtt9POR3WyFvKfn07IN9VkmGPZTHaocOqp5v8xNDyvsB4DWD90X6yvo491JLT08fXhfQyKe7jGLlJ8Ml55X6fAq8G4DVdUNub9UpiM3y5SPb0Bmob9XIdqzbDKTDsMA+Txh8o/xZedfpa21x3BY3iAxyoz+TcXfe9xJ9Cx+s4K4k7wspA3huy0egqd3ezu76JpeTu0/LLkU/wu01tLLlj11pZGYY9w3vcPky6NrYH8Iu4+hYZXcNWZhrj2MGyO8nlKom4HvbqQe13uCgz4IW7x+Kic61JWhCKXm/ayRdH8HnHGSfscMWxn5QWjYAsSb5k5ixzPduW0fi4+GrvJw+tItRPpdnetu/Fw8NXeTh9aRcLapznU3qmbLOy7Lum9URFnAREQBERAEREAREQFZjJ5nf7FUkq0xw8z7XsVOSgPpKh4seQVJuoeKnkFSDzZjfhqjysvruVUrXG/DVHlZfXcqpQDkCpEcg6V0MVzRYe1r4nVTHCJ982mx0yv0HfbfZX0aM6rtH9lpiye03MWyKzOwuMzYdv5K65JSL5Zg2KuZqBwD6R+b2Xmp3D6bSLkN6nDMdYKq5+Vsv8cWd5Qa+cWPetq2W0OfUWvFSa8YyuRLaJXtkcKaW97rM+B7m7UpDWbTQwtJaMjxgFx51gsRsfzqsk4OVBa2oI14kkdrXtKbAr1N0vqVn2Hp8o2i2pruLE+G1Nsrup5g1wvvDXu5QOuTnW6xZVP6Va6FxjqYG7bTZwALSD1tOYX3BMR4ioljvyXETsH1ZbGw69p3bd7dFkGM4TDWMbtM2yRZhaQ14NtoiB5yDrZ8U7kP1Aadq19Wgou6XXXh4omnWjUipoph8LG1rGB3H2O9ihVXDxsmoaO8j1gsV4QcGZKdplaeOpwS0ysaQ6Nw1ZURHlROHmVAHdBy9HuXmntHZv8q90zVCs4PIzSox5r9x7tk/gqasrmnf6AqEjq9i4F5H0nDtzCsqbfvLIultzas+vg76qS9/ctp/Fw8NXeTh9aRagkecrkEFbf8Ai4eGrvJw+tIuRXnvSuc2vLelc3qiIqSkIiIAiIgCIiAIiICox48z7XsVKSrjhCfB/a9ioyUByuomKnkFSLqJih5BUg85Y34ao8rL67lVK1xvw1R5WX13Ljg2EvqX7EdrgbWZAv0AX1PV1FeqdOVSSjHNgseD1LGx7XVkZ4t4sw7gTo630stx1Bvnkrmtofk/zMp26WTwUl77JPN5Xi30O7Q7l2U2LWHybEoyW5MbJblN3gG+o6jn0FTHwOpWFr/1igf9LUx9N+zf5j0r6jZ6UKNPdS8b8875YPhwtwUnlqxU3dPLXVePLnp/xKR0D3AQE/rMPzlO/wDaM1LR06XA6bjeq2pYH8pgsyYF7R4k7OezqvfzOCva2i2Axu3eMnapqjPku3RvOvQP+7Xg1DNvay2HPcBI3dFVjNjx0MkzF+s9SrrU0sdH68M+Nnb/ABlg4u3unLfjuvNdIxybc4b8/Pr6R6VY4JJbjB0xPHpao1Wy9za17m3QTk5vc4Lhhkli7raQsEH2e1J3z9+sLHuT/lNGZV8+zFQ1AuRxTYngGxc1vzcgv4x0B3GxWYYLigcNh1nh/J12Q9xG20AjmcY0iWN1+S/aF9VhFM7jcP2d7JXR/wAQDwe4yX+yujAMRAZsvJAbaKQi92QOdeKYZ86KQnucAug2u7fKS91r6ebZzqdWUIyUf0ykudrvd89NLu2KNj1khLmPbLsyvGzDUuADKgNyNNWNOQlFiMwMxcWzAwXGcHime4RN+S1gvxlO7wbyNTCT0+LrrqsjZiALJhUs2mA8XWxjPlZbFdDbTIC9tbDobevxunaQyCrkBaQDR1w0cy3zbJnDqtyu/MaUyoq+5P3yfno/DxtiX0PxFX3Z/fBLNWzsndpY7tpQ7rstfTbUbiyRpa4bjp3FBJ3fgrbFXPjcYK1tyMhJv6iT9IW3/iqOohLOaQ5h0OoXPrUJU7yhilmn+aPitVwksGdJVE0msU/O/g9TjM7lgWGWq3D8XDw1d5OH1pFpeLnLdHxcPDV3k4fWkXOk7u5W8TeyIigBERAEREAREQBERAUnCQ+D+1/KqIlXfCY5R/a/lVASgOV1FxM8grvuouJnkFSDzvjfh6jysvruUmHCqmNjZWNJDgHcnPI5jaGvoUfGvD1HlZPXcrmi4R7NtoEdn/S6H4fCm5NzlutWs8sevknu/qJlBwlbI3iq2PbbzbnnN79e437FaUdFLADJh0gmhOb6d2tugdPd6F0R1dJU5ShpPTo7zjPzr6ODmydujqC06hrjl/E33LvLetjjzVuufN53KZwbd07+z69fIQmN4f8AJm3afD0L8nDpdFfeOr/7VVDRkWnjGOBjBORezfBJ4srdWk7wp2I1T8vl8DmvbzaqHVp3Eubk7vzUWok2g55cyQHJ0zBZrwNBUsGcbxukAyWadTTrrx+uMRxa0a69ORUV4vyr3vmXWttEjn23bQFz9aN6qojsvPf6VcTi2RvY57tTnnbI3sDcZG197lTVIs5czaHipcGWyV0ZHwcnvBURHeGvHmew/wAiqoaoMl2nDkOuHjpZJk8doNz2gLjg0+y89bHD0g+9dNbqe0+Z2f4rV2t9lg1mn75mFUl2008pW+EvoZbh9Y9rdttnT0jOKkbuqMOOWm/ZuB1AtP0VZmqiDBDIdugqPnKeQ6wOcbCNx0bZ2WXNPUcsMwzEnRGKdubovm3tOj4T9F3UQSzzK7dJFC50Mjr0FSOPgfa5ikcLDzDkOHYVdGtGSxyzV+Dyv4Puy+jStyqtB0qm6lnirZ3We7b9Ub78eMJShordVe50X6rV8qMXEM1uUwDIA9LOlu7ULFX2BIBuLm3R2qXXYpJI1sbjdjMm31I+jtHfZV+0ubtm0Qm7RyWuvNX1jww+77uz05wj3s3nbK/FLS+bWV/M5NHKC3P8W/w1d5OH1pFpaPULdPxb/DV3k4fWkXPZeb2REUAIiIAiIgCIiAIiICh4UnKL7X8qx26yDhWcovtfyrHboDldRcSPIKkXUXEjyFIPP2L/AN4qPKy+u5RWjtUrGP7xUeVl9dyiNt1+haKGR4kSI/zkp9NVSM5riOwkejRVrXjr8/8A0u5sw6PSV0qdSxRJF/DwhnbkSHDrA9llDnrInHa4sxv8eI2840KrvlI6L95965Nlc7JsfnzXqdTewzJipZI5ySt3OBHZsa/V0HdYdV81ArM8+hTpGW55bfxWNBPeXXsoz5LZhoy5VrX86yVce7114mrs5xV54fJDgfYg9f45LtqHX7wskocMbUs26dzWv3xvaHNv9U2LgPOoNaJacgT07ADzXbALXDpa9uR7lMYKMLOWD1th8/NitJOWeJTQyFt7DUEEHrRz3kBpJLW32QSbC+eQ3XVg+oifqwj91wHoIuuL4KfxnjtA9pC8S2eUopRldeNsy3slmpR+P+yS9yt4tOLUx1JHuce8H3LpdTjc5UyoSjml6ni3M4MZmty/Fv8ADV3k4fWkWm2xkHVbj+Lh4eu8nF60izzWOQRvdEReSQiIgCIiAIiIAiIgMe4W6Rfa/lWN3WR8L9IvtfyrG7oD7dRsRPJUi6i4ieQpBoLGv7xUeVl9dyhNYT/3kpuNZVFR5WX13KLGLq2lbIKO8cmQ9J83vKkw019Bf0/9BdbXtHWUfVOOV7Dq/Oa1KpCPV/2+S+FKCxliTHNjZzjc+K32nQdyGoc4G1o49Da/cCdXnqHoUSNoyuDnkGjnO6uoKwYyxztcZWHNZfVjPrdLlfBVKvJdZ/bBctTRGrCKww5L6vP7+66TEGgkjTQe/r9A/GJEwubITrsX9IUnEDo3z+bTzWHeOhScJptqKoPQxo85JKtpbN/M3OT+OkcrbdqW7dZK3ykQsExAwPDhpvHS3fkdenzrZdNiLJIy5zmcWbbXGjai5XNFQDmGusQ2cWzu14Dm3OpyC11/z+dFeYPibonABwaDfYL82jayfHIPpROtsuHU12qy0lKOHDPw6/2WpKRkuI8D6WdxbA40tTqYZTtMd1wv3jcsLxfBaildszMc0eNq09hWXSysMZ2mONOw7L473mon9LHavh3joC7343JAwMq2iqpHcyYZusdP3Sr6lCEry/K9bdYrn/lZ4GtwhKPPrXVf3Y/16GtxJ+Rl+C5cZ+Tmsur+DcM7TLQybQ1MZ5ze0fkdaxSpo3xmz2kLHVp1aau8VxWK+5mcDix2e5bj+Lf4eu8nF6z1piLVbn+LePn64/6cXrPWScrnlG+ERF5JCIiAIiIAiIgCIiArcZw/j47DnA7Tb9O8HtWIy0MrTZ0b7/uk+kZFbARAa8+TSfs3/wALvcuupoZHNI4t/wDC73LY6IDzPws4BVb5nzU0D3bZ2ns2S0h55zml1g4E52vcEnK2ax7+xOJ/5Cp+6K9cogPI39h8T/yFT90V9/sRin+QqfuivXCILnkgcDsVBuKGqB0vxTr27V9HBDFRpR1f3bt+q9bIvaqTWr9WRY8jngbih1oao/8Aqd2rmzgpi7QQ2krADqAx4B7QNV61RFUmndSfqw4xeDR5GPAzEzrQVP3Lvcvn9jMT/wAhU7/8F2/I7uoL10i8uT4knkyLgxjDTdtJWA2DLhjwdgaNuN3UuMXBXF2tLG0dYGHVojeGm+txovWqKVOaybF2eR4eCeKsN2UVW09LYng+cLlLwcxZ/PpKx370ch/FetkRTksm/UHkem4DYnI6zaGe58aPYHeXWW/fgp4FOwunfxxBqJiHSWzDQ0WawHfa5z6Ss7ReQEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAf/2Q==',
                'fabricante_id' => 3, 
                'categoria_id' => 10, 
                'especificacion_id' => 11, 
                'existencia_id' => 11, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],
            [   
                //12
                'nombre_dispositivo' => 'Poco f3',
                'precio' => 4009.00,
                'image'=> 'https://xiaomionline.cl/media/catalog/product/cache/2b88132ea045bae0fc2b44a4f558f9b1/-/_/-_imagen_102-_ce501xia98.jpg',
                'fabricante_id' => 1, 
                'categoria_id' => 1, 
                'especificacion_id' => 12, 
                'existencia_id' => 12, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],
            [   
                //13
                'nombre_dispositivo' => 'iPhone 12',
                'precio' => 10209.00,
                'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTm2qSKXz6_OH1rYzJiTOazgN9eKuG_p5PS00V_XwDJyAMikHbVLjAraztvLUnrNeIragw&usqp=CAU',
                'fabricante_id' => 3, 
                'categoria_id' => 1, 
                'especificacion_id' => 13, 
                'existencia_id' => 13, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //14
                'nombre_dispositivo' => 'Samsung C27F390FHNSAMSUNG',
                'precio' => 2609.00,
                'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjnesPs9KAl0bUGW0_spFGHDyaOlbyN_0oMw0xQ2Z6FMhKfVOEPjXyh2Jht7YN5GKy8oE&usqp=CAU',
                'fabricante_id' => 7, 
                'categoria_id' => 6, 
                'especificacion_id' => 14, 
                'existencia_id' => 14, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //15
                'nombre_dispositivo' => 'Google Pixel 5',
                'precio' => 15549.00,
                'image'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5rnutY9SuNGNXXRxKRiIxG-lJub0WDYyAew&usqp=CAU',
                'fabricante_id' => 10, 
                'categoria_id' => 1, 
                'especificacion_id' => 15, 
                'existencia_id' => 15, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //16
                'nombre_dispositivo' => 'Huawei P40 Pro',
                'precio' => 15009.00,
                'image'=> 'https://m.media-amazon.com/images/I/610uWzNVzRL.jpg',
                'fabricante_id' => 11, 
                'categoria_id' => 1, 
                'especificacion_id' => 16, 
                'existencia_id' => 16, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //17
                'nombre_dispositivo' => 'Xiaomi Mi 11',
                'precio' => 13109.00,
                'image'=> 'https://i.blogs.es/273b41/xiaomi-mi-11-00/1366_2000.jpg',
                'fabricante_id' => 1, 
                'categoria_id' => 1, 
                'especificacion_id' => 17, 
                'existencia_id' => 17, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //18
                'nombre_dispositivo' => 'Sony Xperia 1 III',
                'precio' => 15309.00,
                'image'=> 'https://p.turbosquid.com/ts-thumb/TD/K7VBHU/F5/sx1iiig_0000/jpg/1623683079/600x600/fit_q87/13ae19e1dc5d637a8d095b7944d7afa96581950b/sx1iiig_0000.jpg',
                'fabricante_id' => 5, 
                'categoria_id' => 1, 
                'especificacion_id' => 18, 
                'existencia_id' => 18, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //19
                'nombre_dispositivo' => 'iPhone SE',
                'precio' => 5009.00,
                'image'=> 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAREBMSEBAPEhAXFRUQEBAPDxAQEA8QFRUWFxUVFRUYHSggGBslGxUVITEhJikuLi4uFx81ODMsNzQtLisBCgoKDg0OGBAPFi0dHSAtKy0tLS0tLS0tKy0tLTIrLS0rKzc3LSstLS0tKystKy0rLS0rLS0rKy4rLTcrKzAtLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABAYFBwECCAP/xABGEAACAQICBAoFCAgGAwAAAAAAAQIDEQQhBRIxUQYHEyJBYXGBssEUMjNzkSM0QlJydKGxJCU1YoLC0fAVQ1NjkuFkotL/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EABwRAQEAAgIDAAAAAAAAAAAAAAABAhEhMRJRYf/aAAwDAQACEQMRAD8A3iAAAAAAAAAAAAAAAAAAAAAABsAD413O3NcU/wB6Ll5oj6Lx3KqSkkqkHqzS2PrQE4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4bANnUHIHEjEaL1PSK2rdP6SexvLNGWmYLQ0n6biVlZatt+zPyAsIAAAAAAAAAAAAAAAAAAAAAAcSkkrvYs32Acg17w54yf8AD6UakaCqOcnGnCU3BuMXZylJX1emys72ew+nAfjVwOkWqUv0bFPZRqyTjUf+3UyUn1Oz6mBfgAAAOs5W/wCwOWzqdJVMrrPzO6A5AAHEjBaHpL0zES+k7Reb2KzWXezOzMHof53iO1eFAZ8AAAAAAAAAAAAAAAAAAAAAI+kPZSu7K1m9ye0kEPS8rUKj3RuBoPjt1dTC6mtq8nTa1tucsTmaoNq8cq+Twm7k6XixRquUbAbS4AccWIwmrRx+vicNklVvfEUV2v2kep59fQb90LpnD4yjGthasKtKWyUXse6SecZdTzPGEbdN9mVt/QZXg1wjxej6yrYSrKnLLXjtp1Yr6M47JLb1q+VgPZJq7jo0xOKpYSG2pFz1F/m1HLUpR61rX77E7gBxrYTSGrRrauGxjy5OUvkqz/2pvpf1XnuvtLfpDQmHr1adWrShOpTUo05yjFygpetqtq6va2W9gd9D4dRpQyvLUjHXfrSSu7t9bbfeTjhKxyByAAE9hgNDX9MxOWV4tO63K6sZ6Rg9DfOsR9peFAZ8AAAAAAAAAAAAAAAAAAAAAImlYp0Zp5q1mt6JZF0n7GfYB5/44vZ4T3dLxYo1nY2bxw+zwnu6XixRrzAU9aor7Fzn2LMsSu1bRd2ow9ayunsb8jH16E6ctWcXGW6S2rq3ot+hMPrSc5bNtz64HR8cbUnWqpugr06MbtXS+kn+Jvx4Z8lHZ6t4vNJSlo7B8pKU5OhSbnJuUm3FbW9p5x0zwYq0rypXq0+z5SK60tq618DfHAJtaPwf3el4Ec9NthxknsOTH4SsToyuB3AAHEjBaGi1jMTs1W4233SV/IzszC6I+dV+3yQGdAAAAAAAAAAAAAAAAAAAAACLpS/Izta9srq6v2Eoi6T9lPsA0Bxur5PBp7eSpX6M9bFFEwEebJ77QX5v8i+cb75mD93T8WKKboejrShHrcn+XkajNZepRlyVPD08qlZ2bW2NNZzl5d5acNgVCnGEFzYqy7jHaEpRvVxdS+ovk6S6XThtt2u+e6xW9P6dxFebpRlqrNakHq06UVtu/pS3yezoSzOm2JFnq42hGTi69FSW2KmpSXao3sbP4OqMsJh5QacXSg4ySaTTirM88chGnFJLJq6WyVT96W6O5G/+BL/V2Df/AI1Hs9REzx01jdrFRbRPo1CNQaeX9s71ZqnGU5O0Yxc5PdGKu/yOTTJQlc7GK0Lj+WpQm0o1HGMqlJSUpUZSSlqTtskk1cycWBzIwmiPnVft8kZuRhND/Oq/b5IKzwAAAAAAAAAAAAAAAAAAAAARdKK9Gaz2Wydnt3kojaS9lPs80B5/43Y2p4NdCpUl/wC2KKroSlKbUIevPVpQ6tba+7N9xbON/wBTB+7peLFGO4tcKpVp1perShzfeVLpfCKl8Uax7Zy6ZvhNq4bD8nDKMIxhFfvfR/Jy/hKLhKCjHXkk2+iTyaWdpP6v0pfwosHDTEurUp0k/WbnLqT2PuirmFr1udaNsslfNRs9rXTZ7F0y6kj0Ya5tc71pFxWV9a85y52o8nJfWqfViuiHxN98CnfR2Dvb5vRzX2EaBxMYpc55PNpu8pvfN9PYsl17TfvAj9m4L7tR8COWeW28YstArPGRwjrYWklh1C6s8Q5w17UZXi1FbE+m7vkizUlkVDhLgnVxlSlKLkq1OOqrXvzdRpfAzjOVyvCdxUqj6JUUJRdZ1p1MRneetPKEpdNnGOT6bS6y8xKjxacHqmCwerXSVecm55ptQjzaabXTZa3VrsuCRm9rHE9hg9D/ADqv2+SM5MwWhvndft8okVYAAAAAAAAAAAAAAAAAAAAAAi6T9lPs80SiLpP2U+zzQGgeOD1MJ7un4sUfLgjajo+UumrVk/4YKMF+MZfE+nHB6mE93S8WKMfXramBw0F/p6/fNuX8xvBnJi8bi9atOp021IdV9r7kvxIap5NvJJaz+rFb5eSPlK7moJXk8kvxb/D8DtpvEKFsPB3inylV9M5/RT/O3Wi73Pia5Y/FV9Zt7Esop+tntb6/yPRXAf8AZuC+7UfAjzk6Flee3bq/1PRvAf8AZuC+7UfAjm2s1AnYajFtScYucbqMmlrRUtqT6L2RBpIn4d5lRJ6TudGdkRXEzAaF+eV+3yiZ+ZXtCP8ATMR2/wDyBZAAAAAAAAAAAAAAAAAAAAAAiaU9jPs80SyJpb2M+zzQGgeN/wBTCe7peLFGF0hK8MPHoVKn+EUZnjgfyeE93S8WKK/inrRpr/apxvuTjm+5X+BqJUHAtRc6stzl9mnn+eX4EOhTedWfryblbdcyeNpcyMdjqS1mt1KGxfHUXcyRgsHTadbEZYeHQ/8ANkuj7O/fs3ly9Jj7RtEcH5Yj5as3Chtitkq3Zuj1/Deb74NRh6HhuTtyfI09TVzWrqq1meeNOcIqmIbjHmUdigsnKPXuXUeguAy/VeC+7Ub/APBGGliw+4l0iJRJdMIlo7I6R2HdAcTK5oJ/puI/v6pY5lZ0A/07E9/8oVaQAAAAAAAAAAAAAAAAAAAAAh6X9hPsX5omELTPsKnZ5oDQPG/7PCe7peLFFcw/OVP7EPDbzLDxveywnu6XixRXNETWpBvogvwWfhNY9pUjk+WryztGK1Nb6sIes/8Ak2vgYXhDpTlZKEObQhzYRWx2yu959tKY50qXJxfyk+dUe6+dvxv3ldtcl7I+vKbsz01wDlbRuC+7UfAjzVSo9V2ek+A/7NwX3aj4ERVogiZT2EPDvo+BNogfdbDvE+a2HeICZVuDz/T8T3/ylpmV3Qk6LxtfU11USeupW1XnHOIFlAAAAAAAAAAAAAAAAAAAAACDpv5vU7PNE4gae+bVfs+aA0Bxueywnu6fixRT8BiNSg2+hONu9/1Lfxs+xwn2KfixRripXfJ6m+Tb7LL/ALLAxEnOWtJ5b3te84otN82Pe838DpCDk7syOCoZpLa8luXWBM0dgdfOXqrLPpZ6C4JJeg4XV9XkKWrbZbUVjRGNrKnT1I5ZW67f1ZvXgMv1bgvu1HwItRYqJPokKiibRRlX2O6Otj4YvH06UdapOMV1sD6YusoQlKTskm2VDgA3Vr4nEfRb1Yvfd3/l/Ei6V0rX0k3QwUJOle1Ss01TiuuezuV31Fx0FoqGFoRowztnKVrOc3tf99CQGQAAAAAAAAAAAAAAAAAAAAACFpmKdConJRVs5PNRV1dk0i6VoOpQqQW2UJJdbtl+IHnzjkUFTwqpyc4cnTtJpxvzsT0M1bc2pxh4SVbAQnFXlh5uFRdOrJ8x9l3NdsomqwPtSqb3btM9oymkta6b2XvzUu0rZ9INxV1Npt2sr7F0ssozWNqKU9WHPk9r2RS/vpPQ/AZL/DcFZ3Xo9JX32gkeZ1i504uGqk5Z66vrSi1l3G3+LHjBwsMFTwuJk4VaN4Qb9WpScnKPer6tupC1I29SQxekaVCLlUnGMV0yaSKBieMKFWvSwuDcHWqyVOnKo+am+l2u0ustOA4GQclUxtWWKqbdR3jh4vqh9L+LLqIqGuE2IxjcdH0Jzje3pFRcnRXWpPb3XZLwfA1TlymPrSxM9vJpuFCL7Ns+/LqLVTpqKSikkskkkkluS6DsB0o0owiowjGMUrRjFKMYrckth3AAAAAAAAAAAAAAAAAAAAAAAAAAofCbgtVjWniMNTjVp1E1iMNJJqal61ovKSe7buv0ap0txd4arOXoeI9GrdODxqnHVe6NW2tbdrR/iPSRE0hoyhiI6telTqLo14qTj1p7U+wDyPpvghj8GnKvhpqn/rU7VaNt/KQvFd9mYaEbp5q62Rf0l027N3WesqvA7k25YPE1qD+pNutS7LN6y/5dxS+EvAZTu8VounVvtxWi58nXTf0pU7JSfbGQGgqtVtQTtzIuK32cpSz75M+ZsHH8W0Jt+g42nNp/N8WvR60f3dbOLfbqlR0zwfxmEdsTh6tJbFOUb05fZqK8ZdzAynFi/wBcYH38PM9cHlLifwM62mcJqptQlKtNrZGMIyd336q7z1aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEDSehsNiVavRp1OhSlHnx+zNc5dzMBW4E6qaw2Mr0oPJ0qqjiaVt1pWfxbLcAK7wV4I0MC5zhaVWfrzVOnSilujCCSSv2vrLEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9k=',
                'fabricante_id' => 3, 
                'categoria_id' => 1, 
                'especificacion_id' => 19, 
                'existencia_id' => 19, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //20
                'nombre_dispositivo' => 'Steam Deck',
                'precio' => 15009.00,
                'image'=> 'https://i5.walmartimages.com.mx/mg/gm/3pp/asr/9cb01a58-83d2-420d-9efa-ac95196a39c3.935908ea33a747328f24cdf362092a4a.jpeg?odnHeight=612&odnWidth=612&odnBg=FFFFFF',
                'fabricante_id' => 12, 
                'categoria_id' => 3, 
                'especificacion_id' => 20, 
                'existencia_id' => 20, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //21
                'nombre_dispositivo' => 'Logitech G733 LIGHTSPEED',
                'precio' => 2409.00,
                'image'=> 'https://m.media-amazon.com/images/I/71xNjrzG69L._AC_SL1500_.jpg',
                'fabricante_id' => 9, 
                'categoria_id' => 8, 
                'especificacion_id' => 0, 
                'existencia_id' => 0, 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
        ];
        DB::table('dispositivos')-> insert($dispositivo);
    
          
    
       
    
       

    }
}
