<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Fabricante;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FabricanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $fabricante = new Fabricante();


        $fabricante =[

            [
                //1
                'nombre_fabricante' => 'xiaomi', 
                'pais_origen'=> 'China',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],

            [
                //2
                'nombre_fabricante' => 'lenovo', 
                'pais_origen'=> 'China',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //3
                'nombre_fabricante' => 'apple', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //4
                'nombre_fabricante' => 'Microsoft', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //5
                'nombre_fabricante' => 'Sony', 
                'pais_origen'=> 'Japón',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //6
                'nombre_fabricante' => 'Nintendo', 
                'pais_origen'=> 'Japón',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //7
                'nombre_fabricante' => 'Samsung', 
                'pais_origen'=> 'Corea del Sur',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //8
                'nombre_fabricante' => 'Motorola', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //9
                'nombre_fabricante' => 'Logitech', 
                'pais_origen'=> 'Suizo',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //10
                'nombre_fabricante' => 'Google', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //11
                'nombre_fabricante' => 'Huawei', 
                'pais_origen'=> 'China',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //12
                'nombre_fabricante' => 'Steam', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //13
                'nombre_fabricante' => 'LG', 
                'pais_origen'=> 'Corea del Sur',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //14
                'nombre_fabricante' => 'HP', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],
            [   
                //15
                'nombre_fabricante' => 'Dell', 
                'pais_origen'=> 'Estados Unidos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
               
            ],
            [   
                //16
                'nombre_fabricante' => 'Asus', 
                'pais_origen'=> 'Taiwán',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //17
                'nombre_fabricante' => 'Nokia', 
                'pais_origen'=> 'Finlandia',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //18
                'nombre_fabricante' => 'Toshiba', 
                'pais_origen'=> 'Japón',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //19
                'nombre_fabricante' => 'Oppo', 
                'pais_origen'=> 'China',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //20
                'nombre_fabricante' => 'BlackBerry', 
                'pais_origen'=> 'Canadá',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
        ];
        DB::table('fabricantes')-> insert($fabricante);
    }
}
