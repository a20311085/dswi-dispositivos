<?php

namespace Database\Seeders;
use App\Models\Categoria;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;



use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categorias = new Categoria();


        $categorias =[

            [
                //1
                'nombre_categoria' =>'Celulares', 
                'descripcion_categoria' => 'Dipostivos telefono celular',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //2
                'nombre_categoria' =>'Laptops', 
                'descripcion_categoria' => 'Computadora portatil',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //3
                'nombre_categoria' =>'Consolas', 
                'descripcion_categoria' => 'Consola de video juegos',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //4
                'nombre_categoria' =>'Mouses', 
                'descripcion_categoria' => 'Periferico de computadora', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //5
                'nombre_categoria' =>'SmartTVs', 
                'descripcion_categoria' => 'Pantalla de televisión',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //6
                'nombre_categoria' =>'Monitores', 
                'descripcion_categoria' => 'Pantallas para computadoras',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //7
                'nombre_categoria' =>'Teclados', 
                'descripcion_categoria' => 'Periferico de computadora',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //8
                'nombre_categoria' =>'Audifonos', 
                'descripcion_categoria' => 'Dispostivo de audio',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //9
                'nombre_categoria' =>'Bocinas', 
                'descripcion_categoria' => 'Dispostivo de audio externo', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //10
                'nombre_categoria' =>'Tablets', 
                'descripcion_categoria' => 'Dipostivo tableta electronica',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //11
                'nombre_categoria' =>'PCs', 
                'descripcion_categoria' => 'Computadora de escritorio',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //12
                'nombre_categoria' =>'Controles gamer', 
                'descripcion_categoria' => 'Control para videojuegos',  
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //13
                'nombre_categoria' =>'Camaras', 
                'descripcion_categoria' => 'Camara fotografica profecional', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //14
                'nombre_categoria' =>'SmartWatchs', 
                'descripcion_categoria' => 'Reloj intelginte', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //15
                'nombre_categoria' =>'Webcams', 
                'descripcion_categoria' => 'Camara para computadoras',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //16
                'nombre_categoria' =>'Impresoras', 
                'descripcion_categoria' => 'Dispositvos de impresion', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //17
                'nombre_categoria' =>'Hardwares', 
                'descripcion_categoria' => 'Componentes para computadoras',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //18
                'nombre_categoria' =>'Telefonos', 
                'descripcion_categoria' => 'Telefono fijo de casa ', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //19
                'nombre_categoria' =>'Modems', 
                'descripcion_categoria' => 'Routers para conexion de internet',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //20
                'nombre_categoria' =>'Headsets', 
                'descripcion_categoria' => 'Audifonos de diadema',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
        ];
        DB::table('categorias')-> insert($categorias);
    
    }
}
