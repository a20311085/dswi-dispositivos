<?php

namespace Database\Seeders;

use App\Models\Level;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class levelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $level = new Level();

        $level = [
            [  //1
                'code' => 'SU',
                'name' => 'Superuser',
                'description' => 'Todos los acceso posible',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],


            [
                //2
                'code' => 'AD',
                'name' => 'Admin',
                'description' => 'Aceso a los modulos("Dis,Cat,Fab,Esp,Exi")',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //3
                'code' => 'MO',
                'name' => 'Moderado',
                'description' => 'Acceso limitado a los modulos("Dis,Cat,Fab")',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //4
                'code' => 'IN',
                'name' => 'Invitado',
                'description' => 'Acceso a las secciones del landing',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //5
                'code' => 'US',
                'name' => 'Usuario',
                'description' => 'Acceso a landing',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
        ];

        DB::table('levels')->insert($level);
    }
}
