<?php

namespace Database\Seeders;

use App\Models\CallSection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CallsectioniaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $callsection = new CallSection();
        $callsection =[

            [
                //1
                'title'=>'Call to action',
                'paragraph'=>'Lorem ipsum dolor sit amet 
                consectetur adipisicing elit. Natus veniam enim deleniti 
                similique neque magni velit cumque assumenda nam adipisci. 
                Aliquid dolor totam quae vero sunt ipsa mollitia minima autem!
                Lorem ipsum dolor sit amet 
                consectetur adipisicing elit.',
                'image'=>'https://media.es.wired.com/photos/631ec3ef64fe55a038bdc9a8/16:9/w_1280,c_limit/How-to-Choose-a-Laptop-Gear-GettyImages-1235728903.jpg',
            ],

         
        ];
        DB::table('callsections')-> insert($callsection);
    }
}
