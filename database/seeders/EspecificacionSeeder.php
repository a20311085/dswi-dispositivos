<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Especificacion;
use Carbon\Carbon;

class EspecificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $especificacion = new Especificacion();
        $especificacion =[

            [
                //1
                'almacenamiento'=>'64GB/128GB', 
                'ram'=>'3GB/4GB', 
                'pantalla'=>'6.53 pulgadas IPS LCD', 
                'sistema_operativo'=>'MIUI 11 basado en Android 10', 
                'bateria'=>'5020 mAh', 
                'procesador'=>'Mediatek Helio G85', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>' 162.3 x 77.2 x 8.9 mm', 
                'camaras'=>'48MP + 8MP + 2MP + 2MP / Cámara frontal:13MP',
                'dispositivos_id'=>1,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //2
                'almacenamiento'=>'1TB', 
                'ram'=>'4GB', 
                'pantalla'=>'15 pulgadas', 
                'sistema_operativo'=>'Windows', 
                'bateria'=>'5500 mAh', 
                'procesador'=>'i5 13u', 
                'color'=>'Azul, Negro', 
                'dimensiones'=>'3010 x 1510 mm', 
                'camaras'=>'6 MP',
                'dispositivos_id'=>2,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],

            [
                //3
                'almacenamiento'=>'256GB/512GB/1TB', 
                'ram'=>'8GB', 
                'pantalla'=>'6.0 pulgadas OLED 460 ppi.', 
                'sistema_operativo'=>'iOS', 
                'bateria'=>'4100 mAh', 
                'procesador'=>'A16 Bionic', 
                'color'=>'Azul, Negro, blanco, Rojo', 
                'dimensiones'=>'144.7 x 70.4 x 8.0 mm', 
                'camaras'=>' 48MP + 12MP + 12MP ',
                'dispositivos_id'=>3,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //4
                'almacenamiento'=>'1TB SSD', 
                'ram'=>'16GB GDDR6', 
                'pantalla'=>'No aplica', 
                'sistema_operativo'=>'Xbox OS', 
                'bateria'=>'No aplica', 
                'procesador'=>'CPU AMD Zen 2 personalizado', 
                'color'=>'Negro', 
                'dimensiones'=>'301 x 151 x 151 mm', 
                'camaras'=>'No aplica',
                'dispositivos_id'=>4,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //5
                'almacenamiento'=>'825GB SSD', 
                'ram'=>'16GB GDDR6', 
                'pantalla'=>'No aplica', 
                'sistema_operativo'=>'PlayStation OS', 
                'bateria'=>'No aplica', 
                'procesador'=>'CPU AMD Zen 2 personalizado', 
                'color'=>'Blanco y Negro', 
                'dimensiones'=>'390 x 104 x 260 mm', 
                'camaras'=>'Cámara HD integrada',
                'dispositivos_id'=>5,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //6
                'almacenamiento'=>'32GB/64GB', 
                'ram'=>'4GB', 
                'pantalla'=>'Pantalla LCD de 6.2 pulgadas (720p)', 
                'sistema_operativo'=>' Nintendo Switch OS', 
                'bateria'=>'4310 mAh', 
                'procesador'=>'NVIDIA Custom Tegra', 
                'color'=>'Varios colores disponibles para las Joy-Con', 
                'dimensiones'=>'102 x 239 x 13.9 mm (con Joy-Con)', 
                'camaras'=>'Cámara IR de movimiento y cámara frontal',
                'dispositivos_id'=>6,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //7
                'almacenamiento'=>'128GB/512GB', 
                'ram'=>'8GB/12GB', 
                'pantalla'=>' 6.2 pulgadas Dynamic AMOLED 2X', 
                'sistema_operativo'=>'Android 10 con One UI 2.5', 
                'bateria'=>'4000 mAh', 
                'procesador'=>'Snapdragon 865', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'151.7 x 69.1 x 7.9 mm', 
                'camaras'=>'12MP + 64MP + 12MP / Cámara frontal: 10MP',
                'dispositivos_id'=>7,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //8
                'almacenamiento'=>'64GB', 
                'ram'=>'4GB', 
                'pantalla'=>'6.3 pulgadas IPS LCD', 
                'sistema_operativo'=>'Android 9 Pie', 
                'bateria'=>'4000 mAh', 
                'procesador'=>'Qualcomm Snapdragon 665', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'158.4 x 75.8 x 9.1 mm', 
                'camaras'=>'48MP + 16MP + 5MP / Cámara frontal: 25MP',
                'dispositivos_id'=>8,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                
            ],
            [   
                //9
                'almacenamiento'=>'No aplica', 
                'ram'=>'No aplica', 
                'pantalla'=>'No aplica', 
                'sistema_operativo'=>'No aplica', 
                'bateria'=>'300 mAh', 
                'procesador'=>'No aplica', 
                'color'=>'Blanco, Negro', 
                'dimensiones'=>'63gr 51 x 30 mm', 
                'camaras'=>'No aplica',
                'dispositivos_id'=>9,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //10
                'almacenamiento'=>'No aplica', 
                'ram'=>'4GB', 
                'pantalla'=>'55 pulgadas', 
                'sistema_operativo'=>'Android TV', 
                'bateria'=>'No aplica', 
                'procesador'=>'No aplica', 
                'color'=>'Negro', 
                'dimensiones'=>'1100 x 800 x 12mm', 
                'camaras'=>'No aplica',
                'dispositivos_id'=>10,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //11

                'almacenamiento'=>'256GB/512GB/1TB', 
                'ram'=>'8GB', 
                'pantalla'=>'12.0 pulgadas OLED 460 ppi.', 
                'sistema_operativo'=>'iOS', 
                'bateria'=>'6100 mAh', 
                'procesador'=>'A14 Bionic', 
                'color'=>'Gris', 
                'dimensiones'=>'444.7 x 170.4 x 5.0 mm', 
                'camaras'=>' 48MP + 12MP + 12MP ',
                'dispositivos_id'=>11,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //12
                'almacenamiento'=>'128GB', 
                'ram'=>'6GB', 
                'pantalla'=>'Full HD+ AMOLED 6.67 pulgadas ', 
                'sistema_operativo'=>'No aplica', 
                'bateria'=>'5050 mAh', 
                'procesador'=>'Snapdragon 870', 
                'color'=>'Negro', 
                'dimensiones'=>'160.3 x 70.6 x 8.06 mm', 
                'camaras'=>'12MP X 12MP x 64MP',
                'dispositivos_id'=>12,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //13
                'almacenamiento'=>'64GB/128GB/256GB', 
                'ram'=>'No aplica', 
                'pantalla'=>'XDR display of size 6.1 pulgadas', 
                'sistema_operativo'=>'No aplica', 
                'bateria'=>'4100 mAh', 
                'procesador'=>'A14 Bionic chip', 
                'color'=>'Negro, Blanco', 
                'dimensiones'=>'155.3 x 66.6 x 7.06 mm', 
                'camaras'=>'12MP X 10MP x 48MP',
                'dispositivos_id'=>13,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //14
                'almacenamiento'=>'No aplica', 
                'ram'=>'No aplica', 
                'pantalla'=>'Full HD LED-LCD display', 
                'sistema_operativo'=>'No aplica', 
                'bateria'=>'No aplica', 
                'procesador'=>'No aplica', 
                'color'=>'No aplica', 
                'dimensiones'=>'27 pulgadas', 
                'camaras'=>'No aplica',
                'dispositivos_id'=>14,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //15
                'almacenamiento'=>'128GB', 
                'ram'=>'8GB', 
                'pantalla'=>'6.0 pulgadas OLED', 
                'sistema_operativo'=>'Android 11', 
                'bateria'=>'4080 mAh', 
                'procesador'=>'Qualcomm Snapdragon 765G', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'144.7 x 70.4 x 8.0 mm', 
                'camaras'=>'12.2MP + 16MP / Cámara frontal: 8MP',
                'dispositivos_id'=>15,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //16
                'almacenamiento'=>'128GB/256GB/512GB', 
                'ram'=>'8GB', 
                'pantalla'=>'6.58 pulgadas OLED', 
                'sistema_operativo'=>'EMUI 10.1 (basado en Android 10)', 
                'bateria'=>'4200 mAh', 
                'procesador'=>'Kirin 990 5G', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'158.2 x 72.6 x 9 mm', 
                'camaras'=>'50MP + 12MP + 40MP + ToF 3D / Cámara frontal dual: 32MP + ToF 3D',
                'dispositivos_id'=>16,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //17
                'almacenamiento'=>'128GB/256GB', 
                'ram'=>'8GB/12GB', 
                'pantalla'=>'6.81 pulgadas AMOLED', 
                'sistema_operativo'=>'MIUI 12 basado en Android 11', 
                'bateria'=>'4600 mAh', 
                'procesador'=>'Qualcomm Snapdragon 888', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'164.3 x 74.6 x 8.06 mm', 
                'camaras'=>'108MP + 13MP + 5MP / Cámara frontal: 20MP',
                'dispositivos_id'=>17,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //18
                'almacenamiento'=>'256GB', 
                'ram'=>'12GB', 
                'pantalla'=>'6.5 pulgadas OLED 4K HDR', 
                'sistema_operativo'=>'Android 11', 
                'bateria'=>'4500 mAh', 
                'procesador'=>'Qualcomm Snapdragon 888', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'165 x 71 x 8.2 mm', 
                'camaras'=>' 12MP + 12MP + 12MP + ToF 3D / Cámara frontal: 8MP',
                'dispositivos_id'=>18,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //19
                'almacenamiento'=>'64GB/128GB/256GB', 
                'ram'=>'3GB', 
                'pantalla'=>'4.7 pulgadas Retina HD IPS LCD', 
                'sistema_operativo'=>'iOS 13 (actualizable)', 
                'bateria'=>'1821 mAh', 
                'procesador'=>'Apple A13 Bionic', 
                'color'=>'Varios colores disponibles', 
                'dimensiones'=>'138.4 x 67.3 x 7.3 mm', 
                'camaras'=>'12MP / Cámara frontal: 7MP',
                'dispositivos_id'=>19,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [   
                //20
                'almacenamiento'=>'512 GB', 
                'ram'=>'16 GB', 
                'pantalla'=>'1280 x 800px', 
                'sistema_operativo'=>'Windows 11', 
                'bateria'=>'5000 mAh', 
                'procesador'=>'AMD APU with Zen 2', 
                'color'=>'Negro', 
                'dimensiones'=>'338.4 x 87.3 x 9.3 mm', 
                'camaras'=>'No aplica',
                'dispositivos_id'=>20,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
        ];
        DB::table('especificaciones')-> insert($especificacion);
    }
}
