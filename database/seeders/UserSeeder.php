<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        
        $user = new User();

        
        $user =[

            [
                //1
                'name' =>'Francisco Javier',
                'email'=>'pancho_mins@hotmail.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6625096103',
                'level_id'=>'1',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //2
                'name' =>'Marco Antonio',
                'email'=>'marco@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6621234567',
                'level_id'=>'2',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //3
                'name' =>'Carlos Humberto',
                'email'=>'carlos@gmail.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6445556789',
                'level_id'=>'3',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //4
                'name' =>'Cecilia Caro',
                'email'=>'cecilia@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6448765432',
                'level_id'=>'4',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //5
                'name' =>'Luis Moreno',
                'email'=>'luis@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6622345678',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //6
                'name' =>'usuario 1',
                'email'=>'usuario1@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6628765432',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //7
                'name' =>'usuario 2',
                'email'=>'usuario2@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6339876543',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //8
                'name' =>'usuario 3',
                'email'=>'usuario3@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6334567890',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //9
                'name' =>'usuario 4',
                'email'=>'usuario4@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6311234567',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //10
                'name' =>'usuario 5',
                'email'=>'usuario5@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6312345678',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //11
                'name' =>'usuario 6',
                'email'=>'usuario6@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6623334444',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //12
                'name' =>'usuario 7',
                'email'=>'usuario7@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6624445555',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //13
                'name' =>'usuario 8',
                'email'=>'usuario8@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6445556666',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //14
                'name' =>'usuario 9',
                'email'=>'usuario9@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6446667777',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //15
                'name' =>'usuario 10',
                'email'=>'usuario10@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6337778888',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //16
                'name' =>'usuario 11',
                'email'=>'usuario11@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6338889999',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //17
                'name' =>'usuario 12',
                'email'=>'usuario12@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6319990000',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //18
                'name' =>'usuario 13',
                'email'=>'usuario13@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6310001111',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //19
                'name' =>'usuario 14',
                'email'=>'usuario14@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6621112222',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            [
                //20
                'name' =>'usuario 15',
                'email'=>'usuario15@g.com',
                'password'=>'$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e',
                'phone'=>'6622223333',
                'level_id'=>'5',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ],
            
            
        ];
        DB::table('users')-> insert($user);
    }
}
