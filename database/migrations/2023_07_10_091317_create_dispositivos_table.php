<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('dispositivos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_dispositivo');
            $table->decimal('precio', 10, 2);
            $table->text('image');
            $table->string('fabricante_id');
            $table->string('categoria_id');
            $table->string('especificacion_id')->default(0);
            $table->string('existencia_id')->default(0);
            $table->integer('status')->default(1);
            // Otros campos de información del dispositivo
            $table->timestamps();

          
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('dispositivos');
    }
};
