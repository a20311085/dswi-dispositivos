<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('especificaciones', function (Blueprint $table) {
            $table->id();
            $table->string('almacenamiento');
            $table->string('ram');
            $table->string('pantalla');
            $table->string('sistema_operativo');
            $table->string('bateria');
            $table->string('procesador');
            $table->string('color');
            $table->string('dimensiones');
            $table->string('camaras');
            $table->integer('status')->default(1);
            $table->unsignedBigInteger('dispositivos_id')->default(0);
            // Otros campos de información de las especificaciones
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('especificaciones');
    }
};
