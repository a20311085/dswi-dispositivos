<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('firstTime')->default(0);
            $table->string('password')->default('$2y$10$JyX1C.xTiNH7yqrrrdlkIuLuECLK2iQ2cJB2RAPr451QMP6cIL0.e');
            $table->string('phone')->unique()->nullable();
            $table->string('avatar')->default('/storage/image/avatar.png');
            $table->integer('level_id')->default(5);
            $table->integer('status')->default(1);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
