@extends('layouts.plantilla')

@section('subtitulo')
    <h1 style="color: #04ff00">Existencias</h1>
@endsection

@section('action_button')
    <div class="container">
        <div class="row">
            <div class="col">
                <button class="btn btn-dark buttons" data-bs-toggle="modal" data-bs-target="#agregarmodal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-box-arrow-in-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M3.5 6a.5.5 0 0 0-.5.5v8a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5v-8a.5.5 0 0 0-.5-.5h-2a.5.5 0 0 1 0-1h2A1.5 1.5 0 0 1 14 6.5v8a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 14.5v-8A1.5 1.5 0 0 1 3.5 5h2a.5.5 0 0 1 0 1h-2z" />
                        <path fill-rule="evenodd"
                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                    </svg>

                    Agregar</button>


            </div>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <p class="bold">Se han encontrado los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container text-center">
        {{$existencia->links()}}
        <div class="row">

            <div class=" mb-4" id="tablexite">

                <div class="card-body">
                    <table id="datatablesSimple" class="table table-dark">
                        <thead>
                            <tr class="h3 fw-bold" style="color:#40ff00;">
                                <th>Id</th>
                                <th>Dispositivo</th>
                                <th>Disponible</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="h3 fw-bold" style="color:#40ff00;">
                                <th>Id</th>
                                <th>Dispositivo</th>
                                <th>Disponible</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                                
                                
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($existencia as $exite)
                                <tr class="" style="color:white;">

                                    <td>{{ $exite->id }}</td>
                                    <td class="fw-bold">{{$exite->dispositivo->nombre_dispositivo}}</td>
                                    <td>{{ $exite->cantidad_disponible }}</td>
                                    <td><button data-bs-toggle="modal" data-bs-target="#editarmodal{{ $exite->id }}"
                                            class="btn btn-info mb-3" id="buttonUt">Modificar</button>
                                    </td>
                                    <td> <button data-bs-toggle="modal" data-bs-target="#eliminarmodal{{ $exite->id }}"
                                        class="btn btn-danger" id="buttonDt">Eliminar</button></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

 <!-- Add New Modal -->
 <div class="modal fade" id="agregarmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Agregar Existencia</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('existencia.create') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <label for="recipient-name" class="col-form-label">Existencia de Dispositivo:</label>
                            <select name="dispositivos_id" class="form-control" required>
                                <option value="0">Dispositivos</option>
                                @foreach ($dispositivo->where('existencia_id', 0) as $dis)
                                    <option value="{{ $dis->id }}">{{ $dis->nombre_dispositivo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                        </div>
                        <div class="col-md-12">

                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Disponibilidad:</label>
                                <input type="number" class="form-control" id="almacenamiento" name="cantidad_disponible">
                            </div>
                        </div>
                        
                    </div>
                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-dark">Guardar</button>
            </div>
            </form>
        </div>


    </div>
</div>
<!-- Add New Modal -->


<!-- edit Modal -->
    @foreach ($existencia as $exite)
    <div class="modal fade" id="editarmodal{{ $exite->id }}" tabindex="-1" 
        aria-labelledby="exampleModalLabel{{ $exite->id }}" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Modificar Especificación Dispositivo( {{$exite->dispositivo->nombre_dispositivo}} )</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('existencia.update',['id' => $exite->id]) }}" method="POST">
                        @csrf
                    @method('PUT')
                       
                    <div class="row">
                        <div class="col">
                            <label for="recipient-name" class="col-form-label">Dispositivos sin
                                especificación:</label>
                            <select name="dispositivos_id" class="form-control" required>
                                <option value="{{ $exite->dispositivo->id }}">
                                    {{ $exite->dispositivo->nombre_dispositivo }}</option>                                 
                              
                            </select>
                        </div>
                    </div>

                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Disponibilidad:</label>
                                <input type="text" class="form-control" id="almacenamiento" value="{{$exite->cantidad_disponible}}" name="cantidad_disponible">
                            </div>
                        </div>
                        
                    </div>

                

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Modificar</button>
                </div>
    
            </div>
           
            </form>
        </div>
    </div>
    @endforeach
<!-- edit Modal -->
 <!-- borrar Modal -->
 @foreach ($existencia as $exite)
 <div class="modal fade" id="eliminarmodal{{ $exite->id }}" tabindex="-1"
     aria-labelledby="exampleModalLabel{{ $exite->id }}" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Eliminar Existencia del Dispositivo</h5>
                 <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
             </div>
             <form action="{{ route('existencia.delete', ['id' => $exite->id]) }}" method="POST">
                 @csrf
                 @method('PUT')
                 <div class="modal-body">

                     <h5>¿Estas seguro que quieres eliminar la existencia?: {{$exite->dispositivo->nombre_dispositivo }}?</h5>
                     <input style="display: none" type="text" value="0" name="status">
                     
                     <input style="display: none" type="text" value="0" name="existencia_id">

                     <div class="col">
                         <select style="display: none" name="dispositivos_id" class="form-control" required>
                             <option value="{{ $exite->dispositivo->id }}">
                                 {{ $exite->dispositivo->nombre_dispositivo }}</option>                                 
                         </select>
                     </div>

                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                     <button type="sumbit" class="btn btn-danger">Eliminar</button>
                 </div>
             </form>
         </div>
     </div>
 </div>
@endforeach

<!-- edit borrar -->

@endsection
