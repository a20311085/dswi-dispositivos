@extends('layouts.plantilla')

@section('subtitulo')
    <h1 style="color: #00ffaa">Usuarios</h1>
@endsection

@section('action_button')
    <div class="container">
        <div class="row">
            <div class="col">
                <button class="btn btn-dark buttons" data-bs-toggle="modal" data-bs-target="#agregarmodal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-box-arrow-in-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M3.5 6a.5.5 0 0 0-.5.5v8a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5v-8a.5.5 0 0 0-.5-.5h-2a.5.5 0 0 1 0-1h2A1.5 1.5 0 0 1 14 6.5v8a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 14.5v-8A1.5 1.5 0 0 1 3.5 5h2a.5.5 0 0 1 0 1h-2z" />
                        <path fill-rule="evenodd"
                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                    </svg>
                    Agregar</button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="container">
        {{$users->links()}}

        @if ($errors->any())
        <div class="alert alert-danger">
            <p class="bold">Se han encontrado los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            
        
        <div class="row">

            <div class=" mb-4" id="tableuser">

                <div class="card-body">
                    <table id="datatablesSimple" class="table table-dark">
                        <thead>
                            <tr class="h3 fw-bold" style="color:#00ffaa;">
                                <th>Id</th>
                                <th>Avatar</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Teléfono</th>
                                <th>Nivel</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="h3 fw-bold" style="color:#00ffaa;">
                                <th>Id</th>
                                <th>Avatar</th>
                                <th>nombre</th>
                                <th>Email</th>
                                <th>Teléfono</th>
                                <th>Nivel</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($users as $user)
                                <tr class="h5" style="color:#ffffff;">

                                    <td>{{ $user->id }}</td>
                                    <td><img id="avatar" src=" {{ $user->avatar }}" alt=""></td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->level->code }}</td>

                                    <td>

                                        <button id="buttonUt" class="btn btn-info buttons" data-bs-toggle="modal"
                                            data-bs-target="#editarmodal{{$user->id}}"> Modificar
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                class="icon icon-tabler icon-tabler-edit" width="20" height="20"
                                                viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none"
                                                stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                                <path
                                                    d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                                <path d="M16 5l3 3" />
                                            </svg>
                                        </button>

                                    </td>
                                    <td>
                                        <button id='buttonDt' class="btn btn-danger buttons" data-bs-toggle="modal"
                                            data-bs-target="#eliminarmodal{{ $user->id }}"> Borrar
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                class="icon icon-tabler icon-tabler-trash" width="20" height="20"
                                                viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none"
                                                stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <path d="M4 7l16 0" />
                                                <path d="M10 11l0 6" />
                                                <path d="M14 11l0 6" />
                                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Add New Modal -->
    <div class="modal fade" id="agregarmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Nuevo usuario</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('usuario.create') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">

                            {{-- subir imagen --}}
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="formFile" class="form-label">Insertar imagen del avatar</label>
                                    <input class="form-control" type="file" id="formFile" name="avatar" accept="image">
                                  </div>
                            </div>

                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="recipient-name" class="col-form-label">Nivel:</label>
                                    <select name="level_id" class="form-control" required>
                                        <option value="0">Fabricante del dispositivo</option>
                                        @foreach ($level as $lev)
                                            <option value="{{ $lev->id }}">{{ $lev->code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">

                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Nombre:</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="recipient-name" class="col-form-label">Correo Electronico:</label>
                                    <input type="text" class="form-control" id="precio" name="email">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="recipient-name" class="col-form-label">Número de Teléfono:</label>
                                    <input type="number" class="form-control" id="precio" name="phone">
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Guardar</button>
                        </div>

                </div>

                </form>
            </div>
        </div>
    </div>
        <!-- Add New Modal -->

        <!-- edit Modal -->
@foreach ($users as $user )
<div class="modal fade" id="editarmodal{{ $user->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $user->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Modificar Dispositivo( {{$user->name}} )</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('usuario.update',['id' => $user->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                @method('PUT')
                    <div class="row">

                              {{-- subir imagen --}}
                              <div class="col-md-2">
                                <img class="" id="avatarmodal" src="{{$user->avatar}}" alt="">
                              </div>
                             
                              <div class="col-md-10">
                                <div class="mb-3">
                                    <label for="formFile" class="form-label">Modificar imagen del avatar</label>
                                    <input class="form-control" value="" type="file" id="formFile" name="avatar" accept="image">
                                  </div>
                            </div>

                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Nivel:</label>
                                <select name="level_id" class="form-control" required>
                                    <option value="{{$user->level_id}}">{{$user->level->code}}</option>
                                    @foreach ($level as $lev)
                                        <option value="{{ $lev->id }}">{{ $lev->code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nombre:</label>
                                <input type="text" class="form-control" value="{{$user->name}}" name="name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Correo Electronico:</label>
                                <input type="text" class="form-control" id="precio" value="{{$user->email}}" name="email">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Número de Teléfono:</label>
                                <input type="number" class="form-control" id="precio" value="{{$user->phone}}" name="phone">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-dark">Modificar</button>
            </div>

        </div>
       
        </form>
    </div>
</div>


<!-- borrar Modal -->
@foreach ($users as $user)


<div class="modal fade" id="eliminarmodal{{ $user->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $user->id }}" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Eliminar Dispositivo</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('usuario.delete',['id' => $user->id]) }}" method="POST">
            @csrf
            @method('PUT')
        <div class="modal-body">

            <h5>¿Estas seguro que quieres eliminar: {{ $user->name }}?</h5>
                <input style="display: none" type="text" value="0" name="status">
                <input style="display: none" type="text" value="5" name="level_id">

          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          <button type="sumbit" class="btn btn-danger">Eliminar</button>
        </div>
    </form>
      </div>
    </div>
  </div>
@endforeach

<!-- edit borrar -->

@endforeach

    @endsection
