@extends('layouts.plantilla')

@section('subtitulo')
    <h1 style="color: #ffea00">Categorias</h1>
@endsection

@section('action_button')
    <div class="container">
        <div class="row">
            <div class="col">
                <button class="btn btn-dark buttons" data-bs-toggle="modal" data-bs-target="#agregarmodal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-box-arrow-in-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M3.5 6a.5.5 0 0 0-.5.5v8a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5v-8a.5.5 0 0 0-.5-.5h-2a.5.5 0 0 1 0-1h2A1.5 1.5 0 0 1 14 6.5v8a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 14.5v-8A1.5 1.5 0 0 1 3.5 5h2a.5.5 0 0 1 0 1h-2z" />
                        <path fill-rule="evenodd"
                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                    </svg>
                    Agregar</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container text-center">

        {{$categoria->links()}}
        <div class="row">

            <div class=" mb-4" id="tablecate">

                <div class="card-body">
                    <table id="datatablesSimple" class="table table-dark">
                        <thead>
                            <tr class="h3 fw-bold" style="color:#ffea00;">
                                <th>Id</th>
                                <th>nombre</th>
                                <th>Descripcion</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="h3 fw-bold" style="color:#ffea00;">
                                <th>Id</th>
                                <th>nombre</th>
                                <th>Descripcion</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($categoria as $cate)
                                <tr class="h4" style="color:rgb(255, 255, 255);">

                                    <td>{{ $cate->id }}</td>
                                    <td>{{ $cate->nombre_categoria }}</td>
                                    <td>{{ $cate->descripcion_categoria }}</td>
                                    <td>

                                       <button id="buttonUt" class="btn btn-info buttons" data-bs-toggle="modal"
                                        data-bs-target="#editarmodal{{$cate->id}}"> Modificar
                                           <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                               <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                               <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                               <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                               <path d="M16 5l3 3" />
                                           </svg>
                                       </button>
                                       
                                    </td>
                                    <td>
                                        <button id='buttonDt' class="btn btn-danger buttons" data-bs-toggle="modal"
                                        data-bs-target="#eliminarmodal{{$cate->id}}"
                                        > Borrar
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M4 7l16 0" />
                                                <path d="M10 11l0 6" />
                                                <path d="M14 11l0 6" />
                                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                              </svg>
                                        </button>     
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>



    <!-- Add New Modal -->
    <div class="modal fade" id="agregarmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Nuevo Dispositivo</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('categoria.create') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">

                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Nombre Categoria:</label>
                                    <input type="text" class="form-control" id="nombre_categoria"
                                        name="nombre_categoria">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label for="recipient-name" class="col-form-label">Descripción:</label>
                                    <input type="text" class="form-control" id="precio" name="descripcion_categoria">
                                </div>
                            </div>
                        </div>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Guardar</button>
                </div>

            </div>
           
            </form>
        </div>
    </div>
<!-- Add New Modal -->

<!-- edit Modal -->
@foreach ($categoria as $cate )
<div class="modal fade" id="editarmodal{{ $cate->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $cate->id }}" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Modificar Dispositivo( {{$cate->nombre_categoria}} )</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('categoria.update',['id' => $cate->id]) }}" method="POST">
                    @csrf
                @method('PUT')
                    <div class="row">
                        <div class="col-md-12">

                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nombre dispostivo:</label>
                                <input type="text" class="form-control" id="nombre_categoria"
                                    name="nombre_categoria" value="{{ $cate->nombre_categoria }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">Precio:</label>
                                <input type="text" class="form-control" id="descripcion_categoria"  name="descripcion_categoria" value="{{ $cate->descripcion_categoria }}">
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-dark">Modificar</button>
            </div>

        </div>
       
        </form>
    </div>
</div>
@endforeach

<!-- edit Modal -->
<!-- borrar Modal -->
@foreach ($categoria as $cate)


<div class="modal fade" id="eliminarmodal{{ $cate->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $cate->id }}" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Eliminar Dispositivo</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('categoria.delete',['id' => $cate->id]) }}" method="POST">
            @csrf
            @method('PUT')
        <div class="modal-body">

            <h5>¿Estas seguro que quieres eliminar: {{ $cate->nombre_categoria }}?</h5>
                <input style="display: none" type="text" value="0" name="status">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          <button type="sumbit" class="btn btn-danger">Eliminar</button>
        </div>
    </form>
      </div>
    </div>
  </div>
@endforeach

<!-- edit borrar -->


@endsection
