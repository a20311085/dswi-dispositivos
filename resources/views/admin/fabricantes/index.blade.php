@extends('layouts.plantilla')

@section('subtitulo')
    <h1 style="color: #ff0000">Fabricantes</h1>
@endsection

@section('action_button')

    <div class="container">
        <div class="row">
            <div class="col">
                <button class="btn btn-dark buttons" data-bs-toggle="modal" data-bs-target="#agregarmodal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-box-arrow-in-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M3.5 6a.5.5 0 0 0-.5.5v8a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5v-8a.5.5 0 0 0-.5-.5h-2a.5.5 0 0 1 0-1h2A1.5 1.5 0 0 1 14 6.5v8a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 14.5v-8A1.5 1.5 0 0 1 3.5 5h2a.5.5 0 0 1 0 1h-2z" />
                        <path fill-rule="evenodd"
                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                    </svg>
                    Agregar</button>
            </div>
        </div>
    </div>
@endsection

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <p class="bold">Se han encontrado los siguientes errores:</p>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

    <div class="container text-center">
        
        {{$fabricante->links()}}
        
        <div class="row">

            <div class=" mb-4" id="tablefabric">

                <div class="card-body">
                    <table id="datatablesSimple" class="table table-dark">
                        <thead>
                            <tr class="h3 fw-bold" style="color:#ff0000;">
                                <th>Id</th>
                                <th>Fabricante</th>
                                <th>Paises</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="h3 fw-bold" style="color:#ff0000;">
                                <th>Id</th>
                                <th>Fabricante</th>
                                <th>Paises</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($fabricante as $fabric)
                                <tr class="h4" style="color:rgb(255, 255, 255);">

                                    <td>{{ $fabric->id }}</td>
                                    <td>{{ $fabric->nombre_fabricante }}</td>
                                    <td>{{ $fabric->pais_origen }}</td>
                                    <td>

                                       <button id="buttonUt" class="btn btn-info buttons" data-bs-toggle="modal"
                                        data-bs-target="#editarmodal{{$fabric->id}}"> Modificar
                                           <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                               <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                               <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                               <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                               <path d="M16 5l3 3" />
                                           </svg>
                                       </button>
                                       
                                    </td>
                                    <td>
                                        <button id='buttonDt' class="btn btn-danger buttons" data-bs-toggle="modal"
                                        data-bs-target="#eliminarmodal{{$fabric->id}}"
                                        > Borrar
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M4 7l16 0" />
                                                <path d="M10 11l0 6" />
                                                <path d="M14 11l0 6" />
                                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                              </svg>
                                        </button>     
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>

    

<!-- Add New Modal -->
<div class="modal fade" id="agregarmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Nuevo Fabricante</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('fabricante.create') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">

                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nombre Fabricante:</label>
                                <input type="text" class="form-control" id="nombre_fabricante"
                                    name="nombre_fabricante">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">País de origen:</label>
                                <input type="text" class="form-control" id="precio" name="pais_origen">
                            </div>
                        </div>
                    </div>              
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-dark">Guardar</button>
            </div>

        </div>
       
        </form>
    </div>
</div>
<!-- Add New Modal -->

<!-- edit Modal -->
@foreach ($fabricante as $fabric )
<div class="modal fade" id="editarmodal{{ $fabric->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $fabric->id }}" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Modificar Fabricante( {{$fabric->nombre_fabricante}} )</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('fabricante.update',['id' => $fabric->id]) }}" method="POST">
                    @csrf
                @method('PUT')
                    <div class="row">
                        <div class="col-md-12">

                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nombre Fabricante:</label>
                                <input type="text" class="form-control" id="nombre_fabricante"
                                    name="nombre_fabricante" value="{{ $fabric->nombre_fabricante }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label for="recipient-name" class="col-form-label">País de Origen:</label>
                                <input type="text" class="form-control" id="descripcion_categoria"  name="pais_origen" value="{{ $fabric->pais_origen }}">
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-dark">Modificar</button>
            </div>

        </div>
       
        </form>
    </div>
</div>
@endforeach

<!-- edit Modal -->

<!-- borrar Modal -->
@foreach ($fabricante as $fabric)


<div class="modal fade" id="eliminarmodal{{ $fabric->id }}" tabindex="-1" 
    aria-labelledby="exampleModalLabel{{ $fabric->id }}" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Eliminar Dispositivo</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('fabricante.delete',['id' => $fabric->id]) }}" method="POST">
            @csrf
            @method('PUT')
        <div class="modal-body">

            <h5>¿Estas seguro que quieres eliminar: {{ $fabric->nombre_fabricante }}?</h5>
                <input style="display: none" type="text" value="0" name="status">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          <button type="sumbit" class="btn btn-danger">Eliminar</button>
        </div>
    </form>
      </div>
    </div>
  </div>
@endforeach

<!-- edit borrar -->
    @endsection