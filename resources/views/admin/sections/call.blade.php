@extends('layouts.plantilla')

@section('content')
    @foreach ($callsection as $call)
        
    <div class="row py-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="mb-4">Editar call to action</h2>
              
                <form method="POST" action="{{ route('callsections.update') }}">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="title" class="form-label">Título</label>
                        <input type="text" class="form-control mb-1" id="title" name="title" value="{{ $call->title }}">
                    </div>

                    <div class="col-md-12 text-center">
                        <img src="{{$call -> image}}" class="imghero py" alt="...">
                    </div>

                    <div class="mb-3">
                        <label for="numero" class="form-label">Imagen de la sección</label>
                        <input type="text" class="form-control mb-1" id="image" name="image" value="{{ $call->image }}">
                    </div>

                    <div class="mb-3">
                        <label for="parrafo" class="form-label">Párrafo</label>
                        <textarea class="form-control mb-1" id="paragraph" name="paragraph">{{ $call->paragraph}}</textarea>
                    </div>

                    <button type="submit" class="btn btn-dark">Modificar</button>
                </form>

            </div>

        </div>

    </div>
    

    @endforeach
@endsection
