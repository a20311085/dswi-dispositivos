@extends('layouts.app')


@section('content')
    <section id="seccarousel">
        <div class="container-fluid text-center">
            {{-- Inicia Carousel/slider --}}
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="https://cdn.pixabay.com/photo/2017/10/12/22/46/analogue-2846297_1280.jpg"
                            class="d-block w-100 imgcarousel" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://cdn.pixabay.com/photo/2016/01/12/16/38/video-games-1136046_1280.jpg"
                            class="d-block w-100 imgcarousel" alt="...">
                    </div>
                    <div class="carousel-item ">
                        <img src="https://cdn.pixabay.com/photo/2016/10/30/13/46/smart-watch-1783180_1280.jpg"
                            class="d-block w-100 imgcarousel" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            {{-- Termina Carousel/slider --}}

        </div>
    </section>



    <div class="text-center">

        {{-- Inicia Hero --}}
        <section id="sechero">
            <div class="container">
                @foreach ($herosection as $hero)
                    <div class="row py-5 ">
                        <div class="col-md-8">
                            <hr>
                            <h1 class="h1 " style="text-align: start">{{ $hero->title }}</h1>
                            <p class="fs-4" style="text-align: start">{{ $hero->paragraph }}</p>

                        </div>

                        <div class="col-md-4">
                            <img src="{{ $hero->image }}" class="imghero" alt="...">
                        </div>

                    </div>
                    <hr>
                @endforeach
            </div>
        </section>

        {{-- Termina Hero --}}

        {{-- inicio columnas --}}

        <section id="secdispositivos">
            <div class="container justify-content-center">
                <h1 class="mb-5">Dispositivos</h1>

                <div class="row">
                    @foreach ($dispositivo as $dis)
                        <div class="col-md-3">
                            <div class="card shadowcard">
                                <img class="ms-3 mt-3" id="disimg" src="{{ $dis->image }}" alt="">
                                <div class="card-body">
                                    <p class="card-text h4">{{$dis-> nombre_dispositivo}}</p>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>

        </section>


        {{-- Termina columnas --}}

        {{-- Inicia parrafo --}}
        <section>
            <div class="container">
                <div class="row py-1  ">
                    <h1>Titulo</h1>
                    <p class="fs-4 text-j">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque sint
                        eaque asperiores nobis dolores omnis consequatur culpa magnam totam tempore velit,
                        vel ipsum consequuntur laboriosam quisquam possimus ipsa. Doloremque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque sint
                        eaque asperiores nobis dolores omnis consequatur culpa magnam totam tempore velit,
                        vel ipsum consequuntur laboriosam quisquam possimus ipsa. Doloremque.
                    </p>
                </div>
            </div>
        </section>


        {{-- Termina parrafo --}}

        {{-- Inicia call action --}}
        <section id="seccall" class="section-color">
            <div class="container">
                @foreach ($callsection as $call)
                    <div class="row py-5 ">
                        <div class="col">
                            <img style=" width: 35rem;" src="{{ $call->image }}" class="rounded float-start"
                                alt="...">
                        </div>
                        <div class="col">

                            <h1 class="h1 py-3" style="text-align: start">{{ $call->title }}</h1>
                            <p class="fs-3" style="text-align: start">{{ $call->paragraph }}</p>
                            <a href="#" class="btn btn-dark fs-3 buttons" style="">Action</a>

                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        {{-- Termina call action --}}








        {{-- Inicia parrafo --}}

        <section>
            <div class="container">
                <div class="row py-5">
                    <h1>Titulo</h1>
                    <p class="fs-4 text-j">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reprehenderit a ad quisquam distinctio
                        itaque necessitatibus aut doloremque nostrum,
                        cum quis delectus ipsum consequuntur quod minima similique molestiae quam adipisci deleniti!
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque numquam quo, officiis quidem
                        dolore recusandae, ducimus aliquam doloremque magnam voluptas
                        unde labore voluptatibus illo error nemo, assumenda reiciendis harum nulla.
                    </p>
                </div>
            </div>
        </section>

        {{-- Termina parrafo --}}

        {{-- inicio columnas --}}
        <section>
            <div class="container">
                <div class="overflow-hidden  py-5 ">
                    <div class="row ">
                        <div class="col-md-6">
                            <div class="p-3">

                                <img class="imgcardou "
                                    src="https://cdn.pixabay.com/photo/2016/03/26/13/09/cup-of-coffee-1280537_1280.jpg"
                                    alt="...">
                                <p class="py-2 p-2 h4  text-center">Some quick example</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="p-3">

                                <img class="imgcardou "
                                    src="https://cdn.pixabay.com/photo/2015/06/24/15/45/hands-820272_1280.jpg"
                                    alt="...">
                                <p class="py-2 p-2 h4  text-center">Some quick example</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        {{-- Termina columnas --}}

        {{-- inicio columnas round --}}

        <section id="seccolumround" class="section-color">
            <div class="container">
                <div class="row py-5">
                    <h1>Categorías</h1>

                    <div class="col-md-3">

                        <img class="imgcardround"
                            src="https://th.bing.com/th/id/R.f0a31c6b4b6f9db7eaa756fc44b255a2?rik=FhHYX%2ftqb7Ju2w&pid=ImgRaw&r=0"
                            class="card-img-top" alt="...">
                        <h3 class="py-1">Celulares</h3>
                    </div>

                    <div class="col-md-3">

                        <img class="imgcardround"
                            src="https://static.vecteezy.com/system/resources/previews/000/393/044/original/vector-icon-of-computer-laptop-icon.jpg"
                            class="card-img-top" alt="...">
                        <h3 class="py-1">Laptops</h3>

                    </div>
                    <div class="col-md-3">

                        <img class="imgcardround"
                            src="https://www.pngitem.com/pimgs/m/112-1121814_tablet-icon-png-transparent-png.png"
                            class="card-img-top" alt="...">
                        <h3 class="py-1">Tablets</h3>

                    </div>
                    <div class="col-md-3">

                        <img class="imgcardround"
                            src="https://th.bing.com/th/id/OIP.FPYzYVAdDbbUHAJIiqvGUgHaHa?pid=ImgDet&rs=1"
                            class="card-img-top" alt="...">
                        <h3 class="py-1">SmartWatch</h3>

                    </div>
                </div>
            </div>
        </section>


        {{-- Termina columnas --}}

        {{-- inicio columnas parrafos --}}

        <section class="text-j">
            <div class="container">
                <div class="row py-5 ">
                    <h1>Titulo</h1>
                    <hr>
                    <div class="col-md-6">

                        <p class="h4 ">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error?
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error?
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,

                        </p>
                    </div>

                    <div class="col-md-6">

                        <p class="h4">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error?
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error?
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,

                        </p>
                    </div>
                </div>
            </div>
        </section>

        {{-- Termina columnas parrafos --}}

        {{-- inicio columnas fabricantes --}}

        <section id="secfabricantes">
            <div class="container">
                <div class="overflow-hidden py-5">
                    <h1>Fabricantes</h1>
                    <div class="row ">
                        <div class="col-md-3">
                            <div class="p-3">

                                <img class="imgcardou2 "
                                    src="https://i.pinimg.com/originals/17/35/2f/17352fcf0626d3e553839e902fc14f5a.png"
                                    alt="...">
                                <p class="p-2 h4  text-center">Intel</p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3">

                                <img class="imgcardou2" src="https://clipartcraft.com/images/iphone-logo-ios-2.png"
                                    alt="...">
                                <p class="p-2 h4  text-center">Iphone</p>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="p-3">

                                <img class="imgcardou2"
                                    src="https://th.bing.com/th/id/R.06b81f1531065053a4d7163432416589?rik=uQyZ30SHntvTew&pid=ImgRaw&r=0"
                                    alt="...">
                                <p class="p-2 h4  text-center">Android</p>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="p-3">

                                <img class="imgcardou2"
                                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Xiaomi_logo_%282021-%29.svg/512px-Xiaomi_logo_%282021-%29.svg.png"
                                    alt="...">
                                <p class="p-2 h4  text-center">Xiaomi</p>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </section>


        {{-- Termina columnas --}}

        {{-- inicio columnas parrafos --}}
        <section>
            <div class="container">
                <div class="row py-5 text-j">
                    <div class="col-md-4">
                        <h1>Titulo</h1>
                        <p class="fs-4">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <h1>Titulo</h1>
                        <p class="fs-4">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error

                        </p>
                    </div>
                    <div class="col-md-4">
                        <h1>Titulo</h1>
                        <p class="fs-4">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi veniam nulla impedit explicabo
                            exercitationem eos voluptate neque atque dolore quis distinctio,
                            voluptatem tempore voluptatum, necessitatibus laborum, maiores aspernatur facilis error

                        </p>
                    </div>
                </div>
                <hr>
            </div>
        </section>
        {{-- Termina columnas parrafos --}}


        {{-- Inicia formulario --}}

        <section id="secformcontac">
            <div class="container py-5">
                <h2 class="mb-4">Formulario de Contacto</h2>
                <div class="row py-3 text-start">
                    <div class="row justify-content-center">
                        <div class="col-md-12">

                            <form>
                                <div class="mb-3">
                                    <label for="nombre" class="form-label">Nombre</label>
                                    <input type="text" class="form-control inputform" id="nombre" name="nombre"
                                        required>
                                </div>
                                <div class="mb-3">
                                    <label for="numero" class="form-label">Número de telefono</label>
                                    <input type="number" class="form-control inputform" id="numero" name="numero"
                                        required>
                                </div>

                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control inputform" id="email" name="email"
                                        required>
                                </div>
                                <div class="mb-3">
                                    <label for="mensaje" class="form-label">Mensaje</label>
                                    <textarea class="form-control inputform" id="mensaje" name="mensaje" rows="4" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-dark buttons">Enviar</button>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        {{-- Termina formulario --}}
    @endsection
