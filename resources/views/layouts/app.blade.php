<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Scripts -->
    <link rel="stylesheet" href="{{ mix('sass/app.css') }}">
    <script src="{{ mix('js/app.js') }}" defer></script>
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md py-3" id="navbarstyle">
            <div class="container">
                <a class="navbar-brand" style="font-weight: bold; font-size: 25px; color:white; " href="{{route('index')}}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <h1>|</h1>
                <a style="margin-left: 12px; font-size: 20px; color:rgb(162, 162, 162);" class="navbar-brand" href="{{ route('menu') }}">
                    Admin
                </a>

                
                <a style="margin-left: 1px; font-size: 20px; color:rgb(162, 162, 162);" class="navbar-brand" href="{{ route('index') }}">
                    Landing
                </a>
                
                
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                         
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto card text-dark bg-light">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item fs-5" >
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                                <h1 class="fs-3 p-1">|</h1>

                            @if (Route::has('register'))
                                <li class="nav-item fs-5">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown fs-5">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <img id="avataricon" src="   {{ Auth::user()->avatar }}" alt="">
                                 
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item fs-5" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none ">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="">
            @yield('content')
          

        </main>


        <footer class="py-4 footerstyle">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <p class="text-star">Copyright &copy; Your Dispositivos Electronicos 2023</p>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>


    </div>
</body>
</html>
